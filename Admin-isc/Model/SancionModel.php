<?php
require_once '../BD/AccesoDB.php';
class SancionModel {
    //atributos
    private $idsancion;
    private $estado;
    //propiedades
    function getIdSancion() { return $this->idsancion; }
    function setIdSancion($id){
    $this->idsancion = $id;
    }
    function getEstado() { return $this->estado; }
    function setEstado($estado){
    $this->estado = $estado;
    }
    
    //---metodos------
    function sancionesHoy(){
        try{
            $query="call spu_sancionesHoy();";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            return $lista;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    function sancionesTodas(){
        try{
            $query="call spu_sancionesTodas();";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            return $lista;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    function habilitar(){
        try{
            $idsan = $this->getIdSancion();
            $est = $this->getEstado();
            $query="call spu_altaSancion($idsan, '$est');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
        } catch (Exception $e) {
            throw $e;
        }
    }
}