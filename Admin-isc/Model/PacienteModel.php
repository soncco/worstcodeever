<?php
require_once '../BD/AccesoDB.php';
class PacienteModel {
     //atributos
    private $id;
    private $nombres;
    private $fechanacimiento;
    private $dni;
    private $email;
    private $telefono;
    private $codigo;
    private $foto;
    
    //propiedades
    function getId(){ return $this->id; }    
    function setId($cod){
        $this->id = $cod;
    }    
    function getNombres(){ return $this->nombres; }
    function setNombres($nombres){
        $this->nombres = $nombres;
    }
//    function getFechanacimiento($fecha, $formato = "Y/m/d"){ 
//        return date($formato, strtotime($fecha->fechanacimiento)); }
//    function setFechanacimiento($fecha){
//        $this->fechanacimiento = $fecha;}
        
//    function getFechanacimiento(){ 
//     $año = date(y);
//     $mes = date(m);
//     $dia = date(a);
//     $nuevafecha = date( $año, $mes, $dia);
//        return date($nuevafecha->fechanacimiento); }
//    function setFechanacimiento($fecha){
//        $this->fechanacimiento = $fecha;}
        
    
    function getFechanacimiento(){ return $this->fechanacimiento; }
    function setFechanacimiento($fechanacimiento){
        $this->fechanacimiento = $fechanacimiento;
    }
    function getDni(){ return $this->dni; }
    function setDni($dni){
        $this->dni = $dni;
    }
    function getDireccion(){ return $this->direccion; }
    function setDireccion($direccion){
        $this->direccion = $direccion;
    }
    function getEmail(){ return $this->email; }
    function setEmail($email){
        $this->email = $email;
    }
    function getTelefono(){ return $this->telefono; }
    function setTelefono($tel){
        $this->telefono = $tel;
    }
    function getCodigo(){ return $this->codigo; }
    function setCodigo($codigo){
        $this->codigo = $codigo;
    }
    function getFoto(){ return $this->foto; }
    function setFoto($foto){
        $this->foto = $foto;
    }
    //----metodos----------------
    function listar(){        
        try {
            $query="call spu_listarPac();";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            return $lista;
        }catch (Exception $e) {
            throw $e;
        }
    }
    
    function insertar(){
        try {
            $nom = $this->getNombres();
            $fechanac = $this->getFechanacimiento();
            $dni = $this->getDni();
            $email = $this->getEmail();
            $telf = $this->getTelefono();
            $codigo = $this->getCodigo();
            $foto = $this->getFoto();
            $query="call spu_insertarPac('$nom','$fechanac', '$dni', '$email', '$telf', '$codigo', '$foto');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
        }catch (Exception $e) {
            throw $e;
        }
    }
    
    public function recuperarUnPaciente($id){
        try {        
            $query = "call spu_recuperarUnPac('$id');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
        }catch (Exception $e){
            throw $e;
        }
        
    }
    
//    Edita una Categoria 
    public function editar(){
        try { 
            $id = self::getId();
            $nombres = self::getNombres();
            $fechanac = self::getFechanacimiento();
            $dni = self::getDni();    
            $email = self::getEmail();  
            $tel = self::getTelefono();
            $codigo = self::getCodigo(); 
            $foto = self::getFoto(); 
            
            $query="call spu_modificarPac('$id','$nombres', '$fechanac', '$dni', '$email', '$tel', '$codigo', '$foto');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
        }catch (Exception $e) {
            throw $e;
        }  
    }
    
    function eliminar(){
        try { 
            $id = self::getId();
            $query="call spu_eliminarPac('$id');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
        }catch (Exception $e) {
            throw $e;
        }  
    }    
}



