<?php
require_once '../BD/AccesoDB.php';
class EspecialidadModel {
     //atributos
    private $id;
    private $nombres;
    //propiedades
    function getId(){ return $this->id; }    
    function setId($cod){
        $this->id = $cod;
    }    
    function getNombres(){ return $this->nombres; }
    function setNombres($nombres){
        $this->nombres = $nombres;
    }
    //----metodos----------------
    function listar(){        
        try {
            $query="call spu_listarEspecialidad();";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            return $lista;
        }catch (Exception $e) {
            throw $e;
        }
    }
    
    function insertar(){
        try {
            $nombre = $this->getNombres();
            $query="call spu_insertarEsp ('$nombre');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
        }catch (Exception $e) {
            throw $e;
        }
    }
    
    public function recuperarUnaEspecialidad($id){
        try {        
            $query = "call spu_recuperarUnaEsp('$id');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
        }catch (Exception $e){
            throw $e;
        }
        
    }
    
    //Edita una Categoria 
    public function editar(){
        try { 
            $id = self::getId();
            $nombres = self::getNombres();              
            $query="call spu_modificarEsp('$id','$nombres');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
        }catch (Exception $e) {
            throw $e;
        }  
    }
    
    function eliminar(){
        try { 
            $id = self::getId();
            $query="call spu_eliminarEsp('$id');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
        }catch (Exception $e) {
            throw $e;
        }  
    }    
}



