<?php
require_once '../BD/AccesoDB.php';
class ConsultaModel {
    //atributos
    private $idHorario;
    private $usuario;
    private $estado;
    //propiedades
    function getIdHorario(){return $this->idHorario;}
    function setIdHorario($idHorario) {
        $this->idHorario = $idHorario;
    }
    
    function getUsuario(){return $this->usuario;}
    function setUsuario($usuario) {
        $this->usuario = $usuario;
    }
    function getEstado(){return $this->estado;}
    function setEstado($estado) {
        $this->estado = $estado;
    }
    
    //-----metodos-------
    function citasXMedicoHoy() {
        try{
            $user = $this->getUsuario();
            $query ="call spu_citasXMedHoy('$user');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            return $lista;
        } catch (Exception $e) {
            throw $e;
        }
    }
    function citasXMedicoTodas() {
        try{
            $user = $this->getUsuario();
            $query ="call spu_citasXMedTodas('$user');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            return $lista;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    //ESTE METODO INVOCA A UN TRIGGER
    function registrarAsistencia(){
        try{
            $idhor = $this->getIdHorario();
            $est = $this->getEstado();
            $query ="call spu_registrarAsistencia($idhor, '$est');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
