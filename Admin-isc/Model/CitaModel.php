<?php

require_once '../BD/AccesoDB.php';
class CitaModel {
    //atributos
    private $idHorario;
    private $usuario;
    private $idcita;
    
    //propiedades
    
    function getIdHorario(){ return $this->idHorario; }
    function setIdHorario($idHorario){
        $this->idHorario = $idHorario;
    }
    function getUsuario(){ return $this->usuario; }
    function setUsuario($usuario){
        $this->usuario = $usuario;
    }
    function getCita(){return $this->idcita;}
    function setCita($idcita){
    $this->idcita=$idcita;
    }
    // metodos
    function citasXEspecialidad($id){
        try {
            $query="call spu_citasXdia('$id');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            return $lista;
        } catch (Exception $e) {
            throw $e;
        }
    }
    function listarCita(){
    try {
        $query="select * from cita;";
        $db=  AccesoDB::getInstancia();
        $lista=$db->executeQuery($query);
        return $lista;
    } catch (Exception $e) {
    throw $e;
    }
    }
    
    //METODO LISTAR PARA VISUALIZAR TODOS LOS HORARIOS
    //REUTILIZAMOS CODIGO Y PROCEDIMIENTOS QUE YA EXISTEN
    // LISTA ESPECIALIDADES PARA RESERVAR CITAS
    function listar(){
        try {
            $query="call spu_listarEspecialidad();";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            return $lista;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    //REGISTRAR HORARIO
    function insertar(){
        try {
            $idhor = $this->getIdHorario();
            $user = $this->getUsuario();
            
            $query="call spu_insertarCita('$user',$idhor);";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    function listarCitasXPac(){
        try {
            $user = $this->getUsuario();
            $query="call spu_citasXPac('$user');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            return $lista;
        } catch (Exception $e) {
            throw $e;
        }
    }
    function cancelar(){
        try{
        $idcita=  $this->getCita();
        $query="call usp_CancelarCita('$idcita');";
        $db=  AccesoDB::getInstancia();
        $lista=$db->executeQuery($query);
        $rec=$lista[0];
        return $rec;
        }  catch (Exception $e){
        throw $e;
        }
    }

    function reporteCitas($estado) {
        try {
            // Convertir a procedimiento
            $query = sprintf("call spu_citasXEstado('%s')", $estado);
            $db = AccesoDB::getInstancia();
            $lista=$db->executeQuery($query);
            return $lista;
            
        } catch (Exception $e) {
            throw $e;
        }
    }

    //function cancelar(){
//        try {
//            $idnom = self::getIdNom();
//            $dia = self::getDia();
//            $hora = self::getHora();
//            $query="call spu_liberarHorario('$idnom','$dia', '$hora')";
//        $db = AccesoDB::getInstancia();
//        $lista = $db->executeQuery($query);
//        $rec = $lista[0];
//        return $rec;
//        } catch (Exception $e) {
//            throw $e;
//        }
    //}
}