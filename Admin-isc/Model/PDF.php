<?php
require_once '../../fpdf181/fpdf.php';//Libreria PDF
class PDF extends FPDF
{
    function Header()
    {//Ing.  - ISC - UNSAAC
        // Logo - Ubicar el logo en la carpeta /img
        $this->Image('../../img/logo.png',10,8,33);
        // Arial bold 15
        $this->SetFont('Arial','B',15);
        // Movernos a la derecha
        $this->Cell(80);
        // Título - luego del titulo colocamos 0 (sin marco de titulo)
        $this->Cell(30,10,'Relacion',0,0,'C');
        // Salto de línea
        $this->Ln(20);
    }

    // Pie de página
    function Footer()
    {//Ing. Willian Zamalloa - ISC - UNSAAC
        // Posición: a 1,5 cm del final
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Número de página
        $this->Cell(0,10,'Pagina. '.$this->PageNo().'/{nb}',0,0,'C');
    }
    
    // Tabla coloreada
    function FancyTable($header, $data)
    {
        // Colores, ancho de línea y fuente en negrita
        $this->SetFillColor(255,0,0);
        $this->SetTextColor(255);
        $this->SetDrawColor(128,0,0);
        $this->SetLineWidth(.13);
        $this->SetFont('','B');
        // Cabecera
        $w = array(40, 80);//Determina el ancho de cada columna(id,nombres)
        for($i=0;$i<count($header);$i++)
            $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
        $this->Ln();
        // Restauración de colores y fuentes
        $this->SetFillColor(224,235,255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Datos
        $fill = false;
        foreach($data as $row)
        {
            $this->Cell($w[0],6,$row['idespecialidad'],'LR',0,'L',$fill);
            $this->Cell($w[1],6, utf8_decode($row['nombres']),'LR',0,'L',$fill);
            $this->Ln();
            $fill = !$fill;
        }
        // Línea de cierre
        $this->Cell(array_sum($w),0,'','T');
    }
    
    function FancyTableTrab($header, $data)
    {   // Colores, ancho de línea y fuente en negrita
        $this->SetFillColor(255,0,0);
        $this->SetTextColor(255);
        $this->SetDrawColor(128,0,0);
        $this->SetLineWidth(.13);
        $this->SetFont('','B');
        // Cabecera
        $w = array(15, 70, 25, 30, 50);//Determina el ancho de cada columna(id,nombres)
        for($i=0;$i<count($header);$i++)
            $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
        $this->Ln();
        // Restauración de colores y fuentes
        $this->SetFillColor(224,235,255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Datos
        $fill = false;
        foreach($data as $row)
        {
            $this->Cell($w[0],6,$row['idtrabajador'],'LR',0,'L',$fill);
            $this->Cell($w[1],6,utf8_decode($row['nombres']),'LR',0,'L',$fill);
            $this->Cell($w[2],6,$row['dni'],'LR',0,'L',$fill);
            $this->Cell($w[3],6,$row['cargo'],'LR',0,'L',$fill);
            $this->Cell($w[4],6,utf8_decode($row['nomespecialidad']),'LR',0,'L',$fill);
            $this->Ln();
            $fill = !$fill;
        }// Línea de cierre
        $this->Cell(array_sum($w),0,'','T');
    }
    function FancyTablePaci($header, $data)
    {   // Colores, ancho de línea y fuente en negrita
        $this->SetFillColor(255,0,0);
        $this->SetTextColor(255);
        $this->SetDrawColor(128,0,0);
        $this->SetLineWidth(.13);
        $this->SetFont('','B');
        // Cabecera
        $w = array(15, 90, 30, 30, 30, 40);//Determina el ancho de cada columna(id,nombres)
        for($i=0;$i<count($header);$i++)
            $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
        $this->Ln();
        // Restauración de colores y fuentes
        $this->SetFillColor(224,235,255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Datos
        $fill
                
                = false;
        foreach($data as $row)
        {
            $this->Cell($w[0],6,$row['idpaciente'],'LR',0,'L',$fill);
            $this->Cell($w[1],6,utf8_decode($row['nombres']),'LR',0,'L',$fill);
            $this->Cell($w[2],6,$row['fechanacimiento'],'LR',0,'L',$fill);            
            $this->Cell($w[3],6,$row['dni'],'LR',0,'L',$fill);
            $this->Cell($w[4],6,$row['codigo'],'LR',0,'L',$fill);
            $this->Ln();
            $fill = !$fill;
        }// Línea de cierre
        $this->Cell(array_sum($w),0,'','T');
    }
    function FancyTableCita($header, $data)
    {   // Colores, ancho de línea y fuente en negrita
        $this->SetFillColor(255,0,0);
        $this->SetTextColor(255);
        $this->SetDrawColor(128,0,0);
        $this->SetLineWidth(.13);
        $this->SetFont('','B');
        // Cabecera
        $w = array(15, 30, 30, 30, 30, 40,50);//Determina el ancho de cada columna(id,nombres)
        for($i=0;$i<count($header);$i++)
            $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
        $this->Ln();
        // Restauración de colores y fuentes
        $this->SetFillColor(224,235,255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Datos
        $fill
                
                = false;
        foreach($data as $row)
        {
            $this->Cell($w[0],6,utf8_decode($row['nompaciente']),'LR',0,'L',$fill);
            $this->Cell($w[1],6,$row['fecha'],'LR',0,'L',$fill);
            $this->Cell($w[2],6,$row['dia'],'LR',0,'L',$fill);            
            $this->Cell($w[3],6,$row['horainicio'],'LR',0,'L',$fill);
            $this->Cell($w[4],6,$row['estado'],'LR',0,'L',$fill);
            $this->Cell($w[5],6,utf8_decode($row['nomtrabajador']),'LR',0,'L',$fill);
            $this->Cell($w[6],6,utf8_decode($row['nomespecialidad']),'LR',0,'L',$fill);
            $this->Ln();
            $fill = !$fill;
        }// Línea de cierre
        $this->Cell(array_sum($w),0,'','T');
    }

    function FancyTableCita2($header, $data)
    {   // Colores, ancho de línea y fuente en negrita
        $this->SetFillColor(255,0,0);
        $this->SetTextColor(255);
        $this->SetDrawColor(128,0,0);
        $this->SetLineWidth(.13);
        $this->SetFont('','B');
        // Cabecera
        $w = array(64, 30, 30, 33, 55,60);//Determina el ancho de cada columna(id,nombres)
        for($i=0;$i<count($header);$i++)
            $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
        $this->Ln();
        // Restauración de colores y fuentes
        $this->SetFillColor(224,235,255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Datos
        $fill = false;
        if(count($data)>0):
        foreach($data as $row)
        {
            $this->Cell($w[0],6,utf8_decode($row['nompaciente']),'LR',0,'L',$fill);
            $this->Cell($w[1],6,strftime('%d %m %Y', strtotime($row['fecha'])),'LR',0,'L',$fill);
            $this->Cell($w[2],6,$row['dia'],'LR',0,'L',$fill);            
            $this->Cell($w[3],6,$row['horainicio'],'LR',0,'L',$fill);
            //$this->Cell($w[4],6,$row['estado'],'LR',0,'L',$fill);
            $this->Cell($w[4],6,utf8_decode($row['nomtrabajador']),'LR',0,'L',$fill);
            $this->Cell($w[5],6,utf8_decode($row['nomespecialidad']),'LR',0,'L',$fill);
            $this->Ln();
            $fill = !$fill;
        }// Línea de cierre
        endif;
        $this->Cell(array_sum($w),0,'','T');
    }
}
?>

