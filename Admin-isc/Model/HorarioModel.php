<?php
require_once '../BD/AccesoDB.php';
class HorarioModel {
    //Atributos
    private $idNom;
    private $dia;
    private $hora;
    
    //propiedades
    function  getIdNom (){return $this->idNom;}
    function setIdNom($codNom){
        $this->idNom = $codNom;
    }
    
    function getDia (){return $this->dia;}
    function setDia($nombres){
        $this->dia = $nombres;
    } 
    function getHora (){return $this->hora;}
    function setHora($dni){
        $this->hora = $dni;
    }
    //--Metodos--
    function horaXEspecialidad($id){
        try{
            $query="call spu_horasXEspecialidad('$id');";
            $db = AccesoDB::getInstancia();
            $lista= $db->executeQuery($query);
            return $lista;
        } catch (Exception $e) {
            throw $e;
        }
    }
    //    metodo listar para visualizar todos los horarios
//    reutilizamos codigo y procedimiento que ya existen
    function listar(){
        try {
            $query="call spu_listarEspecialidad();";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            return $lista;
        } catch (Exception $e) {
            throw $e;
        }
    }
    function ListaXEsp($id){
        try {
            $query="call spu_trabXEsp('$id');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            return $lista;
        } catch (Exception $e) {
            throw $e;
        }
    }
    function insetar(){
        try {
            $idnom = $this->getIdNom();
            $dia = $this->getDia();
            $hora = $this->getHora();
            $query="call spu_insertarHorario('$idnom','$dia','$hora');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
            
        } catch (Exception $e) {
            throw $e;
        }
    }
    function liberar(){
        try {
            $idnom = self::getIdNom();
            $dia = self::getDia();
            $hora = self::getHora();
            $query="call spu_liberarHorario('$idnom','$dia','$hora');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
            
        } catch (Exception $e) {
            throw $e;
        }
    }
}