<?php
require_once '../BD/AccesoDB.php';
class TrabajadorModel {
     //atributos
    private $id;
    private $nombres;
    private $dni;
    private $direccion;
    private $email;
    private $cargo;
    private $telefono;
    private $foto;
    private $idespecialidad;
    //propiedades
    function getId(){ return $this->id; }    
    function setId($cod){
        $this->id = $cod;
    }    
    function getNombres(){ return $this->nombres; }
    function setNombres($nombres){
        $this->nombres = $nombres;
    }
    function getDni(){ return $this->dni; }
    function setDni($dni){
        $this->dni = $dni;
    }
    function getDireccion(){ return $this->direccion; }
    function setDireccion($direccion){
        $this->direccion = $direccion;
    }
    function getEmail(){ return $this->email; }
    function setEmail($email){
        $this->email = $email;
    }
    function getCargo(){ return $this->cargo; }
    function setCargo($cargo){
        $this->cargo = $cargo;
    }
    function getTelefono(){ return $this->telefono; }
    function setTelefono($tel){
        $this->telefono = $tel;
    }
    function getFoto(){ return $this->foto; }
    function setFoto($foto){
        $this->foto = $foto;
    }
    function getIdEsp(){ return $this->idespecialidad; }    
    function setIdEsp($idesp){
        $this->idespecialidad = $idesp;
    }
    //----metodos----------------
    function listar(){        
        try {
            $query="call spu_listarTrab();";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            return $lista;
        }catch (Exception $e) {
            throw $e;
        }
    }
    
    function insertar(){
        try {
            $nom = $this->getNombres();
            $dni = $this->getDni();
            $dir = $this->getDireccion();
            $email = $this->getEmail();
            $car = $this->getCargo();
            $telf = $this->getTelefono();
            $foto = $this->getFoto();
            $idE=$this->getIdEsp();
            $query="call spu_insertarTrab('$nom','$dni','$dir','$email','$car', '$telf','$foto','$idE');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
        }catch (Exception $e) {
            throw $e;
        }
    }
    
    public function recuperarUnTrabajador($id){
        try {        
            $query = "call spu_recuperarUnTrab('$id');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
        }catch (Exception $e){
            throw $e;
        }
        
    }
    
    //Edita una Categoria 
    public function editar(){
        try { 
            $id = self::getId();
            $nombres = self::getNombres(); 
            $dni = self::getDni();  
            $dir = self::getDireccion();  
            $email = self::getEmail(); 
            $car = self::getCargo();  
            $tel = self::getTelefono();
            $foto = self::getFoto(); 
            $idE = self::getIdEsp();  
            $query="call spu_modificarTrab('$id','$nombres','$dni','$dir','$email','$car','$tel','$foto','$idE');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
        }catch (Exception $e) {
            throw $e;
        }  
    }
    
    function eliminar(){
        try { 
            $id = self::getId();
            $query="call spu_eliminarEsp('$id');";
            $db = AccesoDB::getInstancia();
            $lista = $db->executeQuery($query);
            $rec = $lista[0];
            return $rec;
        }catch (Exception $e) {
            throw $e;
        }  
    }    
}



