<?php
session_start();
require_once '../Model/LoginModel.php';
require_once '../util/Sesion.php';
try {
    //recuperamos la operacion
    $Op = $_REQUEST["Op"];
    $model = new LoginModel();
    switch ($Op) {
    
        case 'Autenticar':  
            
        $model->setEmail($_REQUEST["email"]);
        $model->setPass($_REQUEST["pass"]);
        $recUser = $model->consultarPorUsuario();
        if($recUser["Error"] == 1){
            //throw new Exception("Usuario o Clave incorrecta!!!!");
            Session::setSesion("error", $recUser["Mensaje"]);
            $target = "../View/login.php";
        }
        else{
            Session::setSesion("user", $recUser);
            $target = "../View/admin/dashboard.php";
        }
        break;
    }
    
} catch (Exception $e) {
    Session::setSesion("mensajeErr", $e->getMessage());
}
//Redireccionamos 
header("location: $target");

