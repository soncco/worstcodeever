<?php
session_start();
require_once '../Model/ConsultaModel.php';
require_once '../util/Sesion.php';
try {
    //recuparamos 
    $Op = $_REQUEST["Op"];
    $model = new ConsultaModel();
    switch ($Op) {
    case 'Asistencia':
        //el parametro usuario se manda desde el menu principal.
        $model->setUsuario(trim($_REQUEST["usuario"]));
        $ListaAsistencia = $model->citasXMedicoHoy();
        Session::setSesion("listaAsistencia", $ListaAsistencia);
        $target = "../View/consulta/Consulta.php";
        break;
    case 'Consulta':
        //el parametro usuario se manda desde el menu principal.
        $model->setUsuario(trim($_REQUEST["usuario"]));
        $ListaAsistencia = $model->citasXMedicoTodas();
        Session::setSesion("listaAsitencia1", $ListaAsistencia);
        $target = "../View/consulta/misConsulta.php";
        break;
//    case 'Horario':
//        $nomEsp = $_REQUEST['nomEsp'];
//        $idEsp = $_REQUEST['idEsp'];
//        $CitaEspecialidad = $model->citasXEspecialidad('$idEsp');
//        Session::setSesion("citaEspecialidad", $CitaEspecialidad);
//        
//        $target = "../View/cita/nuevaCita.php?idEsp=".$idEsp."&nomEsp=".$nomEsp;
//        break;
    case 'Guardar':
        $Usuario  = Session::getSesion("user");
        
        $model->setIdHorario(trim($_REQUEST["idcita"]));
        $model->setEstado(trim($_REQUEST["estado"]));
        $msg = $model->registrarAsistencia();
        $target = "ConsultaController.php?Op=Asistencia&usuario=".$Usuario['email'];
        break;
    }
} catch (Exception $e) {
    Session::setSesion("mensajeErr", $e->getMessage());
}
//redireccionamos
header("location: $target");