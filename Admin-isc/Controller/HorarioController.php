<?php
session_start();
require_once '../Model/HorarioModel.php';
require_once '../Model/EspecialidadModel.php';
require_once '../util/Sesion.php';
try {
    //recuperamos la operacion
    $Op = $_REQUEST["Op"];
    $model = new HorarioModel();
    $especialidad = new EspecialidadModel();
    switch ($Op) {
    case 'Listar':
        $ListaEspecialidad = $especialidad->listar();
        Session::setSesion("listaEspecialidad", $ListaEspecialidad);
        $target = "../View/horario/Horario.php";
        break;
    case 'Horario':
        $nomEsp = $_REQUEST['nomEsp'];
        $idEsp = $_REQUEST['idEsp'];
        $HorarioEspecialidad = $model->horaXEspecialidad($idEsp);
        Session::setSesion("horarioEspecialidad", $HorarioEspecialidad);
        $TrabajadorEspecialidad = $model->ListaXEsp($idEsp);
        Session::setSesion("trabajadorEspecialidad", $TrabajadorEspecialidad);
        $target = "../View/horario/nuevoHorario.php?idEsp=".$idEsp."&nomEsp=".$nomEsp;
        break;

    case 'Guardar':
        $nomEsp = $_REQUEST['nomEsp'];
        $idEsp = $_REQUEST['idEsp'];
        $model->setIdNom(trim($_REQUEST["idtrab"]));
        $model->setDia(trim($_REQUEST["dia"]));
        $model->setHora(trim($_REQUEST["hora"]));
        $msg = $model->insetar();
        Session::setSesion("mensaje", $msg);
        $target ="HorarioController.php?Op=Horario&idEsp=".$idEsp."&nomEsp=".$nomEsp;
        break;
    case 'Liberar':
        $nomEsp = $_REQUEST['nomEsp'];
        $idEsp = $_REQUEST['idEsp'];
        $model->setIdNom(trim($_REQUEST["nom"]));
        $model->setDia(trim($_REQUEST["dia"]));
        $model->setHora(trim($_REQUEST["hora"]));
        $msg = $model->liberar();
        Session::setSesion("mensaje", $msg);
        $target = "HorarioController.php?Op=Horario&idEsp=".$idEsp."&nomEsp=".$nomEsp;
          break;
    }
} catch (Exception $e) {
    Session::setSesion("mensajeErr", $e->getMessage());
}
//Redireccionamos
header("location: $target");
