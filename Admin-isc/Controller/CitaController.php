<?php
session_start();
require_once '../Model/CitaModel.php';
require_once '../Model/EspecialidadModel.php';
require_once '../util/Sesion.php';
try {
    // recuperamos la operacion
    $Op = $_REQUEST["Op"];
    $model = new CitaModel();
    $especialidad = new EspecialidadModel();
    switch ($Op) {
    case 'Listar':
        $ListaEspecialidad = $especialidad->listar();
        Session::setSesion("listaEspecialidad", $ListaEspecialidad);
        $target = "../View/cita/Cita.php";
        break;
    case 'ListarCitas':
        $model->setUsuario(trim($_REQUEST["usuario"]));
        $Lista = $model->listarCitasXPac();
        Session::setSesion("listaCitas",$Lista);
        $target="../View/cita/misCitas.php";
        break;
    
    case 'Horario':
        $nomEsp = $_REQUEST['nomEsp'];
        $idEsp = $_REQUEST['idEsp'];
        $CitaEspecialidad = $model->citasXEspecialidad($idEsp);
        Session::setSesion("citaEspecialidad", $CitaEspecialidad);
        
        $target = "../View/cita/nuevaCita.php?idEsp=".$idEsp."&nomEsp=".$nomEsp;
        break;
    
    case 'Guardar':
        $nomEsp = $_REQUEST['nomEsp'];
        $idEsp = $_REQUEST['idEsp'];
        $model->setIdHorario(trim($_REQUEST["idhorario"]));
        $model->setUsuario(trim($_REQUEST["usuario"]));
        $msg = $model->insertar();
        Session::setSesion("mensaje", $msg);
        if ($msg ['Mensaje']=='Cita Insertada!!!'){
            Session::setSesion ("reservado", $msg);
        }
        
//        $target = "CitaController.php?Op=Horario&idEsp=".$idEsp."&nomEsp".$nomEsp;
        $Usuario = Session::getSesion('user');
        $target = "CitaController.php?Op=MisCitas&usuario=".$Usuario['email'];
        break;
    case 'MisCitas':
        //EL PARAMETRO USUARIO SE MANDA DESDE EL MENU PRINCIPAL.
        $model->setUsuario(trim($_REQUEST["usuario"]));
        $Citas = $model->listarCitasXPac();
        Session::setSesion("listaCitas", $Citas);
        $target = "../View/cita/misCitas.php"; 
        break;

    case 'Cancelar':
        $model->setCita($_REQUEST['idcita']);
        $msg=$model->cancelar();
        Session::setSesion("listaCitas",$Citas);
        $target="../View/cita/misCitas.php";
        break;
    
//    case 'Cancelar':
        //AL ESTUDIANTE DE CORRESPONDE CANCELAR CITA
//        $nomEsp = $_REQUEST['nomEsp'];
//        $idEsp = $_REQUEST['idEsp'];
//        $model->setIdNom(trim($_REQUEST["nom"]));
//        $model->setDia(trim($_REQUEST["dia"]));
//        $model->setHora(trim($_REQUEST["hora"]));
//        $msg = $model->liberar();
//        Session::setSesion("mensaje", $msg);
//        $target = "HoararioController.php?OpHorario&idEsp=".$idEsp."&nomEsp=".$nomEsp;
//        break;
      case 'Print':
        $model->setUsuario(trim($_REQUEST["usuario"]));
        $Lista = $model->listarCitasXPac();
        Session::setSesion("print", $Lista);
        $target = "../View/cita/rptCita.php";
        break;
    case 'CitasEstado':
        $estado = $_REQUEST['estado'];
        $reporte = $model->reporteCitas($estado);
        Session::setSesion("reporte", $reporte);
        $target = "../View/cita/rptCitas2.php"; 
        break;


    case 'CitasEstadoLista':
        $estado = $_REQUEST['estado'];
        $reporte = $model->reporteCitas($estado);
        Session::setSesion("lista", $reporte);
        Session::setSesion("estado", $estado);
        $target = "../View/cita/reporteCitas.php"; 
        break;
    }
} catch (Exception $e) {
    Session::setSesion("mensajeErr", $e->getMessage());
}
//REDIRECCIONAMOS
header("location: $target");