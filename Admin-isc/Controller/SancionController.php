<?php
session_start();
require_once '../Model/SancionModel.php';
require_once '../util/Sesion.php';
try {
    //recuperamos la operacion
    $Op =$_REQUEST["Op"];
    $model = new SancionModel();
    switch ($Op) {
    case 'Sanciones':
        $ListaSanciones = $model->sancionesHoy();
        Session::setSesion("listaSanciones", $ListaSanciones);
        $target = "../View/sancion/Sancion.php";
        break;
    case 'Habilitar':
        $model->setIdSancion(trim($_REQUEST["idsancion"]));
        $model->setEstado(trim($_REQUEST["estado"]));
        $msg = $model->habilitar();
        Session::setSesion("mensaje", $msg);
        $target = "../View/sancion/Sancion.php";
        break;
    case 'Todas':
        $TodoSanciones = $model->sancionesTodas();
        Session::setSesion("todoSanciones", $TodoSanciones);
        $target = "../View/sancion/todoSanciones.php";
        break;
    }
} catch (Exception $e) {
    Session::setSesion("mensajeErr", $e->getMessage());
}
//Redireccionamos
header("location: $target");