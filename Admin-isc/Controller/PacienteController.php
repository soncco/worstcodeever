<?php
session_start();
require_once '../Model/PacienteModel.php';
require_once '../util/Sesion.php';
try {
    //recuperamos la operacion
    $Op = $_REQUEST["Op"];
    $model = new PacienteModel();
    switch ($Op) {
    case 'Listar':
        $Lista = $model->listar();
        Session::setSesion("lista", $Lista);
        $target = "../View/paciente/Paciente.php";
        break;
    case 'Nuevo':	
        $ListaPaciente = $model->listar();
        Session::setSesion("listaPaciente", $ListaPaciente);
        $target = "../View/paciente/nuevoPaciente.php";
        break;
    case 'Guardar':
        $model->setNombres($_REQUEST["nombres"]);
        $model->setFechanacimiento($_REQUEST["fechanacimiento"]);
        $model->setDni($_REQUEST["dni"]);
        $model->setEmail($_REQUEST["email"]);
        $model->setTelefono($_REQUEST["telefono"]);
        $model->setCodigo($_REQUEST["codigo"]);
        $ruta = "/SysCitas/Admin-isc/View/paciente/files/";
        if (is_uploaded_file($_FILES['archivo']['tmp_name'])) {
            $nombreDirectorio = $_SERVER['DOCUMENT_ROOT'].$ruta;
            $nombreFichero = $_FILES['archivo']['name'];
            $nombreCompleto = $nombreDirectorio.$nombreFichero;
            //is_file — Indica si el nombre de fichero es un fichero normal
            if (is_file($nombreCompleto)) {
                $idUnico = time();
                $nombreFichero = $idUnico . "-" . $nombreFichero;
            }
            move_uploaded_file($_FILES['archivo']['tmp_name'], $nombreDirectorio.$nombreFichero);
            print ("El fichero fue subido correctamente!!! <br>");
            $model->setFoto($nombreFichero);
        } 
        else{
            print ("No se ha podido subir el fichero <br>");
            $model->setFoto("error.jpg");
        }
        $msg = $model->insertar();
        Session::setSesion("mensaje", $msg);
        $target = "PacienteController.php?Op=Listar";      
        break;
    case 'Ver':
        $id = $_REQUEST["id"];
        $Consulta=$model->recuperarUnPaciente($id);
        $Cod = $Consulta['idpaciente'];
        $Nombre = $Consulta['nombres'];
        $Fechanac = $Consulta['fechanacimiento'];
        $Dni = $Consulta['dni'];
        $Email = $Consulta['email'];
        $Telefono = $Consulta['telefono'];
        $Codigo = $Consulta['codigo'];
        $Foto = $Consulta['foto'];
        $target = "../View/paciente/verPaciente.php?cod=".$Cod."&nom=".$Nombre."&fechanac=".$Fechanac."&dni=".$Dni."&ema=".$Email."&tel=".$Telefono."&codigo=".$Codigo."&foto=".$Foto;
        break;
    case 'Recuperar':
        $id = $_REQUEST["id"];
        $Consulta=$model->recuperarUnPaciente($id);
        $Cod = $Consulta['idpaciente'];
        $Nombre = $Consulta['nombres'];
        $Fechanac = $Consulta['fechanacimiento'];
        $Dni = $Consulta['dni'];
        $Email = $Consulta['email'];
        $Telefono = $Consulta['telefono'];
        $Codigo = $Consulta['codigo'];
        $Foto = $Consulta['foto'];
        $target = "../View/paciente/editarPaciente.php?cod=".$Cod."&nom=".$Nombre."&fechanac=".$Fechanac."&dni=".$Dni."&ema=".$Email."&tel=".$Telefono."&codigo=".$Codigo."&foto=".$Foto; 
        break;
    case 'Editar':
        $model->setId($_REQUEST["cod"]);
        $model->setNombres($_REQUEST["nombres"]);
        $model->setFechanacimiento($_REQUEST["fechanacimiento"]);
        $model->setDni($_REQUEST["dni"]);
        $model->setEmail($_REQUEST["email"]); 
        $model->setTelefono($_REQUEST["telefono"]);
        $model->setCodigo($_REQUEST["codigo"]);
        
        $ruta = "/SysCitas/Admin-isc/View/paciente/files/";
        if (is_uploaded_file($_FILES['archivo']['tmp_name'])) {
            $nombreDirectorio = $_SERVER['DOCUMENT_ROOT'].$ruta;
            $nombreFichero = $_FILES['archivo']['name'];
            $nombreCompleto = $nombreDirectorio.$nombreFichero;
            //is_file — Indica si el nombre de fichero es un fichero normal
            if (is_file($nombreCompleto)) {
                $idUnico = time();
                $nombreFichero = $idUnico . "-" . $nombreFichero;
            }
            move_uploaded_file($_FILES['archivo']['tmp_name'], $nombreDirectorio.$nombreFichero);
            print ("El fichero fue subido correctamente!!! <br>");
            $model->setFoto($nombreFichero);
        } 
        else{
            print ("No se ha podido subir el fichero <br>");
            $model->setFoto($_REQUEST["foto"]);
        }
        
        $msg = $model->editar();
        Session::setSesion("mensaje", $msg);
        $target = "PacienteController.php?Op=Listar"; 
        break; 
    case 'Eliminar':
        $model->setId($_REQUEST["id"]);
        $msg = $model->eliminar();
        Session::setSesion("mensaje", $msg);
        $target = "PacienteController.php?Op=Listar"; 
        break;
    case 'Print':
        $Lista = $model->listar();
        Session::setSesion("print", $Lista);
        $target = "../View/paciente/rptPaciente.php";
        break;
    case 'Print1':
        $Lista = $model->listar();
        Session::setSesion("print", $Lista);
        $target = "../View/paciente/rptPaciente1.php";
        break;
    }    
} catch (Exception $e) {
    Session::setSesion("mensajeErr", $e->getMessage());
}
//Redireccionamos 
header("location: $target");
