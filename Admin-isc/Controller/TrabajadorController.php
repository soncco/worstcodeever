<?php
session_start();
require_once '../Model/TrabajadorModel.php';
require_once '../Model/EspecialidadModel.php';
require_once '../util/Sesion.php';
try {
    //recuperamos la operacion
    $Op = $_REQUEST["Op"];
    $model = new TrabajadorModel();
    $especialidad = new EspecialidadModel();
    switch ($Op) {
    case 'Listar':
        $Lista = $model->listar();
        Session::setSesion("lista", $Lista);
        $target = "../View/trabajador/Trabajador.php";
        break;
    case 'Nuevo':	
        $ListaEspecialidad = $especialidad->listar();
        Session::setSesion("listaEspecialidad", $ListaEspecialidad);
        $target = "../View/trabajador/nuevoTrabajador.php";
        break;
    case 'Guardar':
        $model->setNombres($_REQUEST["nombres"]);
        $model->setDni($_REQUEST["dni"]);
        $model->setDireccion($_REQUEST["direccion"]);
        $model->setEmail($_REQUEST["email"]);
        $model->setCargo($_REQUEST["cargo"]);
        $model->setTelefono($_REQUEST["telefono"]);
        $ruta = "/SysCitas/Admin-isc/View/paciente/files/";
        if (is_uploaded_file($_FILES['archivo']['tmp_name'])) {
            $nombreDirectorio = $_SERVER['DOCUMENT_ROOT'].$ruta;
            $nombreFichero = $_FILES['archivo']['name'];
            $nombreCompleto = $nombreDirectorio.$nombreFichero;
            //is_file — Indica si el nombre de fichero es un fichero normal
            if (is_file($nombreCompleto)) {
                $idUnico = time();
                $nombreFichero = $idUnico . "-" . $nombreFichero;
            }
            move_uploaded_file($_FILES['archivo']['tmp_name'], $nombreDirectorio.$nombreFichero);
            print ("El fichero fue subido correctamente!!! <br>");
            $model->setFoto($nombreFichero);
        } 
        else{
            print ("No se ha podido subir el fichero <br>");
            $model->setFoto("error.jpg");
        }
        
        $model->setIdEsp($_REQUEST["idesp"]);        
        $msg = $model->insertar();
        Session::setSesion("mensaje", $msg);
        $target = "TrabajadorController.php?Op=Listar";      
        break;
    case 'Ver':
        $id = $_REQUEST["id"];
        $Consulta=$model->recuperarUnTrabajador($id);
        $Cod = $Consulta['idtrabajador'];
        $Nombre = $Consulta['nombres'];
        $Dni = $Consulta['dni'];
        $Direccion = $Consulta['direccion'];
        $Email = $Consulta['email'];
        $Cargo = $Consulta['cargo'];
        $Telefono = $Consulta['telefono'];
        $Foto = $Consulta['foto'];
        $Especialidad = $Consulta['nomespecialidad'];
        
        $target = "../View/trabajador/verTrabajador.php?cod=".$Cod."&nom=".$Nombre."&dni=".$Dni."&dir=".$Direccion."&ema=".$Email."&cargo=".$Cargo."&tel=".$Telefono."&foto=".$Foto."&esp=".$Especialidad; 
        break;
    case 'Recuperar':
        $id = $_REQUEST["id"];
        $Consulta=$model->recuperarUnTrabajador($id);
        $Cod = $Consulta['idtrabajador'];
        $Nombre = $Consulta['nombres'];
        $Dni = $Consulta['dni'];
        $Direccion = $Consulta['direccion'];
        $Email = $Consulta['email'];
        $Cargo = $Consulta['cargo'];
        $Telefono = $Consulta['telefono'];
        $Foto = $Consulta['foto'];
        $idEsp = $Consulta['idespecialidad'];
        $ListaEspecialidad = $especialidad->listar();
        Session::setSesion("listaEspecialidad", $ListaEspecialidad);
        $target = "../View/trabajador/editarTrabajador.php?cod=".$Cod."&nom=".$Nombre."&dni=".$Dni."&dir=".$Direccion."&ema=".$Email."&cargo=".$Cargo."&tel=".$Telefono."&foto=".$Foto."&idesp=".$idEsp; 
        break;
    case 'Editar':
        $model->setId($_REQUEST["cod"]);
        $model->setNombres($_REQUEST["nombres"]);
        $model->setDni($_REQUEST["dni"]);
        $model->setDireccion($_REQUEST["direccion"]);
        $model->setEmail($_REQUEST["email"]);
        $model->setCargo($_REQUEST["cargo"]);
        $model->setTelefono($_REQUEST["telefono"]);
//        $model->setTelefono($_REQUEST["telefono"]);
        $ruta = "/SysCitas/Admin-isc/View/trabajador/files/";
        if (is_uploaded_file($_FILES['archivo']['tmp_name'])) {
            $nombreDirectorio = $_SERVER['DOCUMENT_ROOT'].$ruta;
            $nombreFichero = $_FILES['archivo']['name'];
            $nombreCompleto = $nombreDirectorio.$nombreFichero;
            //is_file — Indica si el nombre de fichero es un fichero normal
            if (is_file($nombreCompleto)) {
                $idUnico = time();
                $nombreFichero = $idUnico . "-" . $nombreFichero;
            }
            move_uploaded_file($_FILES['archivo']['tmp_name'], $nombreDirectorio.$nombreFichero);
            print ("El fichero fue subido correctamente!!! <br>");
            $model->setFoto($nombreFichero);
        } 
        else{
            print ("No se ha podido subir el fichero <br>");
            $model->setFoto($_REQUEST["foto"]);
        }
        
        
        $model->setIdEsp($_REQUEST["idesp"]);
        
        $msg = $model->editar();
        Session::setSesion("mensaje", $msg);
        $target = "TrabajadorController.php?Op=Listar"; 
        break;
    case 'Eliminar':
        $model->setId($_REQUEST["id"]);
        $msg = $model->eliminar();
        Session::setSesion("mensaje", $msg);
        $target = "EspecialidadController.php?Op=Listar"; 
        break;
    case 'Print':
        $Lista = $model->listar();
        Session::setSesion("print", $Lista);
        $target = "../View/trabajador/rptTrabajador.php";
        break;    
    }    
} catch (Exception $e) {
    Session::setSesion("mensajeErr", $e->getMessage());
}
//Redireccionamos 
header("location: $target");