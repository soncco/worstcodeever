<?php
session_start();
require_once '../Model/EspecialidadModel.php';
require_once '../util/Sesion.php';
try {
    //recuperamos la operacion
    $Op = $_REQUEST["Op"];
    $model = new EspecialidadModel();
    switch ($Op) {
    case 'Listar':
        $Lista = $model->listar();
        Session::setSesion("lista", $Lista);
        $target = "../View/especialidad/Especialidad.php";
        break;
    case 'Guardar':
        $model->setNombres($_REQUEST["nombres"]);
        $msg = $model->insertar();
        Session::setSesion("mensaje", $msg);
        $target = "EspecialidadController.php?Op=Listar";      
        break;
    case 'Ver':
        $id = $_REQUEST["id"];
        $Consulta=$model->recuperarUnaEspecialidad($id);
        $Cod = $Consulta['idespecialidad'];
        $Nombre = $Consulta['nombres'];
        $target = "../View/especialidad/verEspecialidad.php?cod=".$Cod."&nom=".$Nombre; 
        break;
    case 'Recuperar':
        $id = $_REQUEST["id"];
        $Consulta=$model->recuperarUnaEspecialidad($id);
        $Cod = $Consulta['idespecialidad'];
        $Nombre = $Consulta['nombres'];
        $target = "../View/especialidad/editarEspecialidad.php?cod=".$Cod."&nom=".$Nombre; 
        break;
    case 'Editar':
        $model->setId($_REQUEST["cod"]);
        $model->setNombres($_REQUEST["nombres"]);
        $msg = $model->editar();
        Session::setSesion("mensaje", $msg);
        $target = "EspecialidadController.php?Op=Listar"; 
        break;
    case 'Eliminar':
        $model->setId($_REQUEST["id"]);
        $msg = $model->eliminar();
        Session::setSesion("mensaje", $msg);
        $target = "EspecialidadController.php?Op=Listar"; 
        break;
    case 'Print':
        $Lista = $model->listar();
        Session::setSesion("print", $Lista);
        $target = "../View/especialidad/rptEspecialidad.php";
        break;
    case 'Print1':
        $Lista = $model->listar();
        Session::setSesion("print", $Lista);
        $target = "../View/especialidad/rptEspecialidad1.php";
        break;
    }    
} catch (Exception $e) {
    Session::setSesion("mensajeErr", $e->getMessage());
}
//Redireccionamos 
header("location: $target");



