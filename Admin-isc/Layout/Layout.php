<?php
/**
 * Description of Layout
 *
 * @author JONAZ
 */
class Layout {

    function menu($jsm = "", $Usuario, $reservado = 100) {
        ?>
        <!DOCTYPE html>
        <html lang="en">

            <head>

                <meta charset="utf-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="description" content="">
                <meta name="author" content="">

                <title>SB Admin 2 - SysCitas</title>

                <!-- Bootstrap Core CSS -->
                <link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

                <!-- MetisMenu CSS -->
                <link href="../../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

                <!-- Timeline CSS -->
                <!--<link href="../../dist/css/timeline.css" rel="stylesheet">-->

                <!-- Custom CSS -->
                <link href="../../dist/css/sb-admin-2.css" rel="stylesheet">

                <?php echo $jsm; ?>
                <!-- Morris Charts CSS -->
                <!--<link href="../../bower_components/morrisjs/morris.css" rel="stylesheet">-->

                <!-- Custom Fonts -->
                <link href="../../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

                <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
                <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                <!--[if lt IE 9]>
                    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
                <![endif]-->

            </head>

            <body>

                <div id="wrapper">

                    <!-- Navigation -->
                    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="../../View/admin/dashboard.php">SysCitas Admin v2.0</a>
                        </div>
                        <!-- /.navbar-header -->

                        <ul class="nav navbar-top-links navbar-right">


                            <!-- /.dropdown -->
                            <li><a href="../../View/configuration/widget-default.php">Bienvenido <?php echo $Usuario['email']. ' (' .$Usuario['tipo'] . ')'; ?></a></li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <li><a href="#"><i class="fa fa-user fa-fw"></i> Perfil de Usuario</a>
                                    </li>
                                    <li><a href="../../View/configuration/widget-default.php"><i class="fa fa-gear fa-fw"></i> Ajustes</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li><a href="../salir.php"><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesión</a>
                                    </li>
                                </ul>
                                <!-- /.dropdown-user -->
                            </li>
                            <!-- /.dropdown -->
                        </ul>
                        <!-- /.navbar-top-links -->

                        <div class="navbar-default sidebar" role="navigation" style="height: 100vh">
                            <div class="sidebar-nav navbar-collapse">
                                <ul class="nav" id="side-menu">
                                    <li class="sidebar-search">

                                        <!-- /input-group -->
                                    </li>
                                        <?php if($Usuario['tipo']=="ADMINISTRADOR"){?>
                                    <li>
                                        <a href="../../View/admin/dashboard.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                                    </li>
                                    
                                    <li>
                                        <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Mantenimiento<span class="fa arrow"></span></a>
                                        <ul class="nav nav-second-level">
                                            <li>
                                                <a href="../../Controller/EspecialidadController.php?Op=Listar">Especialidad</a>
                                            </li>
                                            <li>
                                                <a href="../../Controller/TrabajadorController.php?Op=Listar">Trabajador</a>
                                            </li>
                                            <li>
                                        <a href="../../Controller/PacienteController.php?Op=Listar">Paciente</a>
                                    </li>
                                        </ul>
                                        <!-- /.nav-second-level -->
                                    </li>
                                    <?php }?>
                                    <?php if($Usuario['tipo']=="ADMINISTRADOR"){?>
                                    <li>
                                        <a href="../../Controller/HorarioController.php?Op=Listar"><i class="fa fa-table"></i> Horarios</a>
                                    </li>
                                    <?php }?>
                                    <?php if($Usuario['tipo']=="PACIENTE"){?>
                                    <li>
                                        <a href="#"><i class="fa fa-edit fa-fw"></i> Citas<span class="fa arrow"></span></a>
                                        <ul class="nav nav-second-level">
                                            <?php
                                            if(($Usuario['condicion']=="HABILITADO") && ($reservado > 0 )){ ?>
                                            <li>
                                                <a href="../../Controller/CitaController.php?Op=Listar">Reservar</a>
                                            </li>
                                            <?php }?>
                                            <li>
                                                <a href="../../Controller/CitaController.php?Op=MisCitas&usuario=<?php echo $Usuario['email'];?>">Mis Citas</a>
                                            </li>
                                            <li>
                                                <a href="../../Controller/CitaController.php?Op=Sancion">Sanciones</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav-second-level -->
                                    </li>
                                    <?php }?>
                                    <?php if($Usuario['tipo']=="MEDICO"){?>
                                    <li>
                                        <a href="#"><i class="fa fa-edit fa-fw"></i> Consultas<span class="fa arrow"></span></a>
                                        <ul class="nav nav-second-level">
                                            <li>
                                                <a href="../../Controller/ConsultaController.php?Op=Asistencia&usuario=<?php echo $Usuario['email'];?>">Registrar Asistencia</a>
                                            </li>
                                            <li>
                                                <a href="../../Controller/ConsultaController.php?Op=Consulta&usuario=<?php echo $Usuario['email'];?>">Todas Mis Consultas</a>
                                            </li>
                                            <li>
                                                <a href="#">Mis Horarios</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav-second-level -->
                                    </li>
                                    <?php }?>
                                    <?php if($Usuario['tipo']=="ADMINISTRADOR"){?>
                                    <li>
                                        <a href="#"><i class="fa fa-edit fa-fw"></i> Sanciones<span class="fa arrow"></span></a>
                                        <ul class="nav nav-second-level">
                                            <li>
                                                <a href="../../Controller/SancionController.php?Op=Sanciones">Habilitar Sanciones</a>
                                            </li>
                                            <li>
                                                <a href="../../Controller/SancionController.php?Op=Todas">Todas las Sanciones</a>
                                            </li>
                                        </ul>
                                        <!-- /.nav-second-level -->
                                    </li>
                                    <?php }?>
                                    <?php if($Usuario['tipo']=="ADMINISTRADOR"){?>
                                    <li>
                                        <a href="#"><i class="fa fa-bar-chart fa-fw"></i> Reportes<span class="fa arrow"></span></a>
                                        <ul class="nav nav-second-level">
                                            
                                            <li>
                                                <a href="../../Controller/CitaController.php?Op=CitasEstadoLista&estado=CANCELADO">Citas Canceladas</a>
                                            </li>
                                            <li>
                                                <a href="../../Controller/CitaController.php?Op=CitasEstadoLista&estado=ASISTIO">Citas Atendidas</a>
                                            </li>

                                        </ul>
                                        <!-- /.nav-second-level -->
                                    </li>
                                    <?php } ?>
                                    <li>
                                        <!--<a href="forms.html"><i class="fa fa-edit fa-fw"></i> Forms</a>-->
                                    </li>
                                </ul>
                            </div>
                            <!-- /.sidebar-collapse -->
                        </div>
                        <!-- /.navbar-static-side -->
                    </nav>

                    <div id="page-wrapper"> <!-- Begin page-wrapper -->

                        <?php
                    }

function footer($jsf = "") {
    ?>
                    </div>
                    <!-- /END #page-wrapper -->

                </div>
                <!-- /#wrapper -->

                <!-- jQuery -->
                <script src="../../bower_components/jquery/dist/jquery.min.js"></script>

                <!-- Bootstrap Core JavaScript -->
                <script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

                <!-- Metis Menu Plugin JavaScript -->
                <script src="../../bower_components/metisMenu/dist/metisMenu.min.js"></script>

                <!-- Morris Charts JavaScript -->
<!--                <script src="../../bower_components/raphael/raphael-min.js"></script>
                <script src="../../bower_components/morrisjs/morris.min.js"></script>
                <script src="../../js/morris-data.js"></script>-->
                <?php echo $jsf; ?>
                <!-- Custom Theme JavaScript -->
                <script src="../../dist/js/sb-admin-2.js"></script>
                <!-- Agregado Recientemente para el Data Table -->
                <script>
                $(document).ready(function() {
                    $('#dataTables-example').DataTable({
                            responsive: true
                    });
                });
                </script>
            </body>
        </html>
        <?php
    }
}
?>
