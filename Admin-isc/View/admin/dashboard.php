<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
if(Session::NoExisteSesion("user") ) {
    header("location: ../login.php");
    return;
}
if (Session::existeSesion("reservado")) {
    // $reserva = Session::getSesion("reservado");
    $reservo = 0;
    
}
 else {
     $reservo = 100;
}
$Usuario = Session::getSesion("user");
//Llamamos al menu
Layout::menu("", $Usuario, $reservo);
//Solo el contenido que cambiara ira aqui
?>
<div class="row">
    <div class="col-lg-12">

      <div class="page-header">
        <h3>Bienvenido, tiene disponibles las siguientes opciones</h3>
      </div>
      <div class="col-md-6">
        <?php if($Usuario['tipo']=="ADMINISTRADOR") :?>
          <div class="panel panel-success">
            <div class="panel-heading"><i class="fa fa-edit"></i> Mantenimiento</div>
            <ul class="list-group">
              <li class="list-group-item">
                <a href="../../Controller/EspecialidadController.php?Op=Listar">Especialidad</a>
              </li>
              <li class="list-group-item">
                <a href="../../Controller/TrabajadorController.php?Op=Listar">Trabajador</a>
              </li>
              <li class="list-group-item">
                <a href="../../Controller/PacienteController.php?Op=Listar">Paciente</a>
              </li>
              <li class="list-group-item">
                <a href="../../Controller/HorarioController.php?Op=Listar">Horarios de Especialidades</a>
              </li>
            </ul>
          </div>
        <?php endif ?>

        <?php if($Usuario['tipo']=="PACIENTE") :?>
          <div class="panel panel-success">
            <div class="panel-heading"><i class="fa fa-edit"></i> Citas</div>
            <ul class="list-group">
              <li class="list-group-item">
                  <a href="../../Controller/ConsultaController.php?Op=Asistencia&usuario=<?php echo $Usuario['email'];?>">Registrar Asistencia</a>
              </li>
              <li class="list-group-item">
                  <a href="../../Controller/ConsultaController.php?Op=Consulta&usuario=<?php echo $Usuario['email'];?>">Todas Mis Consultas</a>
              </li>
              <li class="list-group-item">
                  <a href="#">Mis Horarios</a>
              </li>
            </ul>
          </div>
        <?php endif ?>

        <?php if($Usuario['tipo']=="MEDICO") :?>
          <div class="panel panel-success">
            <div class="panel-heading"><i class="fa fa-edit"></i> Citas</div>
            <ul class="list-group">
              <?php
              if(($Usuario['habilitado']=="1") && ($reservo > 0 )){ ?>
              <li class="list-group-item">
                  <a href="../../Controller/CitaController.php?Op=Listar">Reservar</a>
              </li>
              <?php }?>
              <li class="list-group-item">
                  <a href="../../Controller/CitaController.php?Op=MisCitas&usuario=<?php echo $Usuario['email'];?>">Mis Citas</a>
              </li>
              <li class="list-group-item">
                  <a href="../../Controller/CitaController.php?Op=Sancion">Sanciones</a>
              </li>
            </ul>
          </div>
        <?php endif ?>
      </div>

      <div class="col-md-6">
        <?php if($Usuario['tipo']=="ADMINISTRADOR") :?>
          <div class="panel panel-info">
            <div class="panel-heading"><i class="fa fa-edit"></i> Sanciones</div>
            <ul class="list-group">
              <li class="list-group-item">
                  <a href="../../Controller/SancionController.php?Op=Sanciones">Habilitar Sanciones</a>
              </li>
              <li class="list-group-item">
                  <a href="../../Controller/SancionController.php?Op=Todas">Todas las Sanciones</a>
              </li>
            </ul>
          </div>
        <?php endif ?>

        <?php if($Usuario['tipo']=="ADMINISTRADOR") :?>
          <div class="panel panel-warning">
            <div class="panel-heading"><i class="fa fa-bar-chart"></i> Reportes</div>
            <ul class="list-group">
              <li class="list-group-item">
                  <a href="../../Controller/CitaController.php?Op=CitasEstadoLista&estado=CANCELADO">Citas Canceladas</a>
              </li>
              <li class="list-group-item">
                  <a href="../../Controller/CitaController.php?Op=CitasEstadoLista&estado=ASISTIO">Citas Atendidas</a>
              </li>
            </ul>
          </div>
        <?php endif ?>
      </div>
    </div>
    <!-- /.col-lg-12 -->
</div>



<?php
//Llamamos al footer y se cierra la pagina
Layout::footer();
?>
