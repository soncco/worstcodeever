<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
if(Session::NoExisteSesion("user") ) {
    header("location: ../login.php");
    return;
}
$Usuario = Session::getSesion("user");
//Llamamos al menu
Layout::menu("", $Usuario);
$url = "../../Controller/PacienteController.php";
$UpperCss = "style='text-transform:uppercase;'";
$UpperJs = "onkeyup='javascript:this.value=this.value.toUpperCase();'";
//Solo el contenido que cambiara ira aqui
?>
<div class="row">
    <div class="col-lg-12">
        <div class="page-header">
        <h3 class="title-header">Modificar Paciente
            <img src="<?php echo "files/".$_REQUEST['foto']?>"  class="img-thumbnail" width="54" height="26">
            <span class="small pull-right">
                <a href="Paciente.php" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-hand-left"></span> Volver 
                </a>
            </span>
        </div>
        </h3>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Modifique la informacion del Paciente
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <form role="form" method="post" enctype="multipart/form-data" action="<?php echo $url; ?>">
                        <div class="form-group">
                        <label>Cod Paciente</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> disabled="" value="<?php echo $_REQUEST['cod']?>" >
                        <input class="hidden-sm"type="text" name="cod" value="<?php echo $_REQUEST['cod']?>" class="form-control" hidden >
                        <input class="hidden-sm"type="text" name="foto" value="<?php echo $_REQUEST['foto']?>" class="form-control" hidden >
                        <input class="hidden-sm"type="text" value="Editar" class="form-control" hidden name="Op" >
                    </div>
                    <div class="form-group">
                        <label>Nombre del Paciente</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="nombres" autofocus type="text" value="<?php echo $_REQUEST['nom']?>">
                    </div>
                    <div class="form-group">
                        <label>Fecha Nacimiento</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="fechanacimiento" type="text" value="<?php echo $_REQUEST['fechanac']?>">
                    </div>
                    <div class="form-group">
                        <label>DNI</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="dni" type="number" value="<?php echo $_REQUEST['dni']?>" >
                    </div>
                    
                    <div class="form-group">
                        <label>Email</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="email" type="email" value="<?php echo $_REQUEST['ema']?>" >
                    </div>                    
                </div>                
                <div class="col-lg-6">                   
                    
                    <div class="form-group">
                        <label>Telefono</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="telefono" type="tel" value="<?php echo $_REQUEST['tel']?>">
                    </div>
                    <div class="form-group">
                        <label>Codigo</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="codigo" type="codigo" value="<?php echo $_REQUEST['codigo']?>">
                    </div> 
                    <div class="form-group">
                        <label>Solo si desea cambiar la Foto</label>
                        <input type="file" name="archivo" >
                    </div>
                    
                    <button type="submit" class="btn btn-success">Guardar</button>
                    <button type="reset" class="btn btn-warning">Reset</button>
                    
                </div>
                </form>
            </div>    
        </div>
    </div>
</div>
<?php
//Llamamos al footer y se cierra la pagina
Layout::footer();
?>         