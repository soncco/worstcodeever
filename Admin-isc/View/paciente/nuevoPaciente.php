<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
if(Session::NoExisteSesion("user") ) {
    header("location: ../login.php");
    return;
}
if(Session::NoExisteSesion("listaPaciente") ) {
    header("location: ../../Controller/PacienteController.php?Op=Nuevo");
    return;
}
$Usuario = Session::getSesion("user");
//Llamamos al menu
Layout::menu("", $Usuario);
$url = "../../Controller/PacienteController.php?Op=Guardar";
$UpperCss = "style='text-transform:uppercase;'";
$UpperJs = "onkeyup='javascript:this.value=this.value.toUpperCase();'";
//Solo el contenido que cambiara ira aqui
$ListaPaciente=  Session::getSesion("listaPaciente");
Session::eliminarSesion("listaPaciente");
$url1 = "Paciente.php";
?>
<div class="row">
    <div class="col-lg-12">
        <div class="page-header">
        <h3 class="title-header">Nuevo Paciente
            <span class="pull-right small">
                <a href="<?php echo $url1;?>" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-hand-left"></span> Volver 
                </a>
            </span>
        </h3>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Ingrese un nuevo Paciente
        </div>
        <div class="panel-body">
            <div class="row">
                <form role="form" method="post" enctype="multipart/form-data" action="<?php echo $url; ?>">
                <div class="col-lg-6">                    
                    <div class="form-group">
                        <label>Nombre del Paciente</label>
                        <input title="Se necesita un nombre" class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="nombres" autofocus type="text" required/>
                    </div>
                    <div class="form-group">
                        <label>Fecha de nacimiento</label>
                        <input title="Se necesita su fecha de nacimiento" class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="fechanacimiento" type="date" required/>
                    </div>
                    <div class="form-group">
                        <label>DNI</label>
                        <input title="Se necesita un DNI" class="form-control" <?php echo $UpperCss.' '.$UpperJs;?>  type="text" name="dni" value="" size="9" maxlength="8"   required/>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input title="Se necesita su correo" class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> type="text" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" name="email"  required/>
                    </div>                    
                                
          
                <div class="form-group">
                        <label>Telefono</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="telefono" type="tel">
                </div>                         
                <div class="form-group">
                        <label>Codigo</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="codigo" type="codigo">
                </div>
                    <div class="form-group">
                        <label>Subir Foto</label>
                        <input type="file" name="archivo" >
                    </div>
                    <button type="submit" class="btn btn-success">Guardar</button>
                    <button type="reset" class="btn btn-warning">Reset</button>
                    
                </div>
                </form>
            </div>    
        </div>
    </div>
</div>
<?php
//Llamamos al footer y se cierra la pagina
Layout::footer();
?>                