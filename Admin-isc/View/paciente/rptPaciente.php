<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Model/PDF.php';;
if(Session::NoExisteSesion("user") ) {
    header("location: ../login.php");
    return;
}
if(Session::NoExisteSesion("print") ) {
    header("location: ../../Controller/PacienteController.php?Op=Listar");
    return;
}
$Lista= Session::getSesion("print");
Session::eliminarSesion("print");
//print_r($Lista);

$pdf = new PDF();
$pdf->AliasNbPages();
// Títulos de las columnas
//$header = array('Id','Nombres','Dni','Direccion','Email','Cargo','Telefono','Especialidad');
$header = array('Id','Nombres','Fecha Nac','Dni','Codigo');
// Carga de datos
$data = $Lista;//$pdf->LoadData('paises.txt');
$pdf->SetFont('Arial','',14);

$pdf->AddPage();
$pdf->FancyTablePaci($header,$Lista);
$pdf->Output();
?>



