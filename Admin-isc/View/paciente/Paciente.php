<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
if(Session::NoExisteSesion("user") ) {
    header("location: ../login.php");
    return;
}
//En el caso de actualizar la pagina web entonces llamaremos nuevamente 
//al Controlador para de esta manera tener actualizada la lista
//y que no me genere ningun error de carga de datos
if(Session::NoExisteSesion("lista") ) {
    header("location: ../../Controller/PacienteController.php?Op=Listar");
    return;
}
$Lista= Session::getSesion("lista");
Session::eliminarSesion("lista");
$Usuario = Session::getSesion("user");
//estas variables se definen en una sola linea
$jsm = "<link href='../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css' rel='stylesheet'>";
$jsm.= "<link href='../../bower_components/datatables-responsive/css/dataTables.responsive.css' rel='stylesheet'>";
//Llamamos al menu
Layout::menu($jsm, $Usuario);
$print = "../../Controller/PacienteController.php?Op=";
?>
<div class="row">
 <div class="col-lg-12">
        <div class="page-header">
        <h3 class="title-header">Paciente
            <span class="small pull-right">
             <a href="<?php echo" ../../View/paciente/nuevoPaciente.php";?>"
               class="btn btn-info btn-sm">
                <span class="glyphicon glyphicon-plus"></span>Agregar
            </a>
            <a href="<?php echo $print."Print" ;?>" class="btn btn-success btn-sm" target="_blank">
                <span class="glyphicon glyphicon-print"></span> Imprimir 
            </a></span>
        </h3>
        </div>
        <?php
        if (Session::existeSesion("mensaje")){
            $mensaje = Session::eliminarSesion("mensaje");
            if ($mensaje['Error'] == 0){
        ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo "NRO DE ERRORES: ".$mensaje['Error'].",   MENSAJE: ".$mensaje['Mensaje']?>
            </div>    
        <?php
            }
            else{
        ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo "NRO DE ERRORES: ".$mensaje['Error'].",   MENSAJE: ".$mensaje['Mensaje']?>
            </div>   
        <?php
            }
        }
        ?> 
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Relacion de Pacientes
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>IdPaciente</th>
                                <th>Nombres</th>
<!--                                <th>FechaNacimiento</th>-->
                                <th>Dni</th>
                                <th>Email</th>                                
                                                               
                                <!--<th>Foto</th>-->                                
                                <th>Operaciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  
                            $url = "../../Controller/PacienteController.php?id=";
                            foreach ($Lista as $row ) { 
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $row['idpaciente']?></td>
                                <td><?php echo $row['nombres']?></td>
                                
                                <td><?php echo $row['dni']?></td>                                
                                <td><?php  echo $row['email']?></td>                                
                                                               
                               
                                
                                <td class="center">
                                    <ul class="nav nav-pills">
                                        <li>
                                            <a href="<?php echo $url . $row['idpaciente'] ?>&Op=Ver" title="Ver">
                                                <span class="glyphicon glyphicon-search"></span> 
                                            </a>
                                        </li>
                                       <li>
                                            <a href="<?php echo $url . $row['idpaciente'] ?>&Op=Recuperar" title="Editar">
                                                <span class="glyphicon  glyphicon-pencil"></span>
                                            </a>                                            
                                        </li>
                                        <li>
                                            <a href="<?php echo $url . $row['idpaciente'] ?>&Op=Eliminar" title="Eliminar"
                                               onclick="return confirm('Se eliminara este registro, ¿Estas Seguro?');">
                                                <span class="glyphicon glyphicon-trash"></span> 
                                            </a>
                                        </li>
                                    </ul>  
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->                
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
//Llamamos al footer y se cierra la pagina
$jsf = "<script src='../../bower_components/DataTables/media/js/jquery.dataTables.min.js'></script>";
$jsf.="<script src='../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js'></script>";
Layout::footer($jsf);
?>