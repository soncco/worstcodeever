<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Model/PDF.php';;
if(Session::NoExisteSesion("user") ) {
    header("location: ../login.php");
    return;
}
if(Session::NoExisteSesion("print") ) {
    header("location: ../../Controller/EspecialidadController.php?Op=Listar");
    return;
}
$Lista= Session::getSesion("print");
Session::eliminarSesion("print");
//print_r($Lista);

//Generamos el PDF iNSTANCIANDO LA CLASE
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',12);
//Listamos de la BD
foreach ($Lista as $row ) { 
    $id = $row['idespecialidad'];
    $nom = $row['nombres'];
    $pdf->Cell(0,10,"$id"." -- "."$nom",0,1);    
}
$pdf->Output();
?>



