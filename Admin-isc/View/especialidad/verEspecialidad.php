<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
if(Session::NoExisteSesion("user") ) {
    header("location: ../login.php");
    return;
}
$Usuario = Session::getSesion("user");
//Llamamos al menu
Layout::menu("", $Usuario);
$url = "Especialidad.php";
//Solo el contenido que cambiara ira aqui
?>
<div class="row">
    <div class="col-lg-12">
        <div class="page-header">
        <h3 class="title-header">Ver Especialidad
            <span class="small pull-right">
        <a href="<?php echo $url;?>" class="btn btn-info btn-sm">
            <span class="glyphicon glyphicon-hand-left"></span> Volver 
        </a>
        </span>
        </h3>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Solo se visualiza la Especialidad
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <form role="form" method="" action="">
                        <div class="form-group">
                            <label>Cod de la Especialidad</label>
                            <input class="form-control" disabled="" value="<?php echo $_REQUEST['cod']?>">
                        </div>
                        <div class="form-group">
                            <label>Nombre de la Especialidad</label>
                            <input class="form-control" disabled="" value="<?php echo $_REQUEST['nom']?>">
                        </div>  
                    </form>
                </div>
            </div>    
        </div>
    </div>
</div>
<?php Layout::footer(); ?>       

