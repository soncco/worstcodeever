<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
if(Session::NoExisteSesion("user") ) {
    header("location: ../login.php");
    return;
}
$Usuario = Session::getSesion("user");
//Llamamos al menu
Layout::menu("", $Usuario);
$url = "../../Controller/EspecialidadController.php";
$UpperCss = "style='text-transform:uppercase;'";
$UpperJs = "onkeyup='javascript:this.value=this.value.toUpperCase();'";
//Solo el contenido que cambiara ira aqui
?>
<div class="row">
    <div class="col-lg-12">
        <div class="page-header">
            <h3 class="title-header">Modificar Especialidad
                <span class="pull-right small">
                    <a href="Especialidad.php" class="btn btn-info btn-sm">
                        <span class="glyphicon glyphicon-hand-left"></span> Volver 
                    </a>
            </span>
            </h3>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Modifique la especialidad Especialidad
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-lg-6">
                    <form role="form" method="post" action="<?php echo $url?>">
                        <div class="form-group">
                            <label>Codigo de la  Especialidad</label>
                            <input class="form-control"  value="<?php echo $_REQUEST['cod']?>" disabled="">
                            <input class="hidden-sm"type="text" name="cod" value="<?php echo $_REQUEST['cod']?>" class="form-control" hidden >
                            <input class="hidden-sm"type="text" value="Editar" class="form-control" hidden name="Op" >
                        </div>   
                        <div class="form-group">
                            <label>Nombre de la Especialidad</label>
                            <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="nombres" value="<?php echo $_REQUEST['nom']?>" autofocus>
                            <p class="help-block">Ejemplo: Medicina General, Psicologia, etc.</p>
                        </div> 
                        <button type="submit" class="btn btn-success" onclick="return confirm('Estas Seguro?');return false;">Guardar</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </form>
                </div>
            </div>    
        </div>
    </div>
</div>
<?php
//Llamamos al footer y se cierra la pagina
Layout::footer();
?>     