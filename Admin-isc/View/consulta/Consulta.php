<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
if(Session::NoExisteSesion("user") ) {
    header("location: ../login.php");
    return;
}
//En el caso de actualizar la pagina web entonces llamaremos nuevamente 
//al Controlador para de esta manera tener actualizada la lista
//y que no me genere ningun error de carga de datos
if(Session::NoExisteSesion("listaAsistencia") ) {
    $Usuario = Session::getSesion("user");
    header("location: ../../Controller/ConsultaController.php?Op=Asistencia&usuario=".$Usuario['email']);
    
    return;
}
$Lista = Session::getSesion("listaAsistencia");
Session::eliminarSesion("listaAsistencia");
$Usuario = Session::getSesion("user");
//estas variables se definen en una sola linea
$jsm = "<link href='../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css' rel='stylesheet'>";
$jsm.= "<link href='../../bower_components/datatables-responsive/css/dataTables.responsive.css' rel='stylesheet'>";
//Llamamos al menu
Layout::menu($jsm, $Usuario);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="page-header">
        <h3 class="title-header">Asistencia de Pacientes para Hoy</h3>
        </div>
        <?php
        if (Session::existeSesion("mensaje")) {
            $mensaje = Session::eliminarSesion("mensaje");
            if ($mensaje['Error']== 0) {
              ?>
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <?php echo "NRO DE ERRORES: ".$mensaje['Error'].", MENSAJE: ".$mensaje['Mensaje']?>
        </div>
        <?php
            }
            else {
                ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-didden="true">x</button>
            <?php echo "NRO DE ERROR: ".$mensaje['Error'].", MENSAJE: ".$mensaje['Mensaje']?>
            
        </div>
        <?php
            }

        }
        ?>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Registrar la Asistencia de los Pacientes.
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Paciente</th>
                                <th>Fecha</th>
                                <th>Dia</th>
                                <th>Hora</th>
                                <th>Estado</th>
                                <th>Medico</th>
                                <th>Especialidad</th>
                                <th>Asistencia</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $url = "../../Controller/ConsultaController.php?Op=Guardar&idcita=";
                            foreach ($Lista as $row ) {
                                ?>
                            <tr class="odd gradeX">
                            <td><?php echo $row['nompaciente']?></td>
                            <td><?php echo $row['fecha']?></td>
                            <td><?php echo $row['dia']?></td>
                            <td><?php echo $row['horainicio']?></td>
                            <td><?php echo $row['estado']?></td>
                            <td><?php echo $row['nomtrabajador']?></td>
                            <td><?php echo $row['nomespecialidad']?></td>
                            <td class="center">
                                <ul class="nav nav-pills">
                                    <?php if($row ['estado'] == 'RESERVADO') {?>
                                    <li>
                                        <a href="<?php echo $url . $row ['idcita']?>&estado=ASISTIO" 
                                           title="Asistio a la Consulta" class="btn btn-success btn-sm"
                                           onclick="return confirm('¿Se registrara la ASISTENCIA a esta consulta?');" rol="button">
                                            <span class="glyphicon glyphicon-ok"></span></a>
                                    </li>      
                                    <li>
                                        <a href="<?php echo $url . $row ['idcita']?>&estado=FALTO" 
                                           title="Falto ala Consulta" class="btn btn-danger btn-sm"
                                           onabort="return confirm('¿Se registrara una FALTA a esta consulta?');" rol="button">
                                            <span class="glyphicon glyphicon-remove"></span></a>
                                    </li>
                                    <?php } elseif ($row['estado']== 'ASISTIO') {?>
                                    <li>
                                        <a href="#" class="btn btn-success btn-sm disabled" >
                                            <span class="glyphicon glyphicon-thumbs-up"></span>
                                        </a>
                                    </li>
                                    <?php } elseif ($row['estado']== 'FALTO') {?>
                                    <li>
                                        <a href="#" class="btn btn-danger btn-sm disabled" >
                                            <span class="glyphicon glyphicon-thumbs-down "></span>
                                        </a>
                                    </li>
                                    <?php }elseif ($row['estado'] == 'CANCELADO') {?>
                                    <li>
                                        <a href="#" class="btn btn-danger btn-sm disabled ">
                                            <span class="glyphicon glyphicon-thumbs-down"></span>
                                        </a>
                                    </li>
                                    <?php }?>
                                </ul>
                            </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
//Llamamos al footer y se cierra la pagina
$jsf = "<script src='../../bower_components/DataTables/media/js/jquery.dataTables.min.js'></script>";
$jsf.="<script src='../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js'></script>";
Layout::footer($jsf);
?>