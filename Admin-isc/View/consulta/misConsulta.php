<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
if (Session::NoExisteSesion("user")) {
    header("location: ../login.php");
    return;
}

//En el caso de actualizar la pagina web entonces llamaremos nuevamente
//al Controlador para de esta manera tener actualizada la lista
//y que no me genere ningun error de carga de datos
if(Session::NoExisteSesion("listaAsitencia1") ) {
    $Usuario = Session::getSesion("user");
header("location: ../../Controller/ConsultaController.php?Op=Consulta&usuario=".$Usuario['email']);
    return;
}

$Lista = Session::getSesion("listaAsitencia1");
Session::eliminarSesion("listaAsitencia1");
$Usuario = Session::getSesion("user");
//estas variables se definen en una sola linea
$jsm = "<link href='../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css' rel='stylesheet'>";
$jsm.= "<link href='../../bower_components/datatables-responsive/css/dataTables.responsive.css' rel='stylesheet'>";
//Llamamos al menu
Layout::menu($jsm, $Usuario);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="page-header">
            <h3 class="title-header">Todas mis Consultas</h3>
        </div>
    <?php
    if (Session::existeSesion("mensaje")) {
        $mensaje = Session::eliminarSesion("mensaje");
        if($mensaje['Error'] == 0) {
    ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
              <?php echo  "NRO DE ERRORES: ".$mensaje['Error'].", MEMSAJE: ".$mensaje['Mensaje']?>
    </div>
    <?php
        }
        else{
            ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
        <?php echo "NRO DE ERRORES: ".$mensaje['Error'].", MENSAJE: ".$mensaje['Mensaje']?>
    </div>
    <?php
        }
    }
    ?>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Asistencia de los Pacientes
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Paciente</th>
                                <th>Fecha</th>
                                <th>Dia</th>
                                <th>Hora</th>
                                <th>Estado de Cita</th>
                                <th>Medico</th>
                                <th>Especialidad</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($Lista as $row) {
                                    ?>
                            <tr class="odd gradeX">
                                <td><?php echo $row['nompaciente']?></td>
                                <td><?php echo $row['fecha']?></td>
                                <td><?php echo $row['dia']?></td>
                                <td><?php echo $row['horainicio']?></td>
                                <td><?php echo $row['estado']?></td>
                                <td><?php echo $row['nomtrabajador']?></td>
                                <td><?php echo 
                                $row['nomespecialidad']?></td>
                                
                            </tr>
                                <?php } ?>
                        </tbody>
                    </table>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <?php
    //llamamos al footer y se cierra la pagina
    $jsf ="script src='../../bower-components/DataTables/media/js/jquery.dataTables.min.js'></script>";
$jsf .="<script src='../../bower-components/dattables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js'></script>";
Layout::footer($jsf);
?>
