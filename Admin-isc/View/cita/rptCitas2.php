<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Model/PDF.php';;
if(Session::NoExisteSesion("user") ) {
    header("location: ../login.php");
    return;
}
if(Session::NoExisteSesion("reporte") ) {
    header("location: ../../Controller/CitaController.php?Op=ListarCitas");
    return;
}
$Lista= Session::getSesion("reporte");
//Session::eliminarSesion("reporte");
//print_r($Lista);

$pdf = new PDF();
$pdf->AliasNbPages();
// Títulos de las columnas
//$header = array('Id','Nombres','Dni','Direccion','Email','Cargo','Telefono','Especialidad');
$header = array('Paciente','Fecha','Dia','Hora','Medico','Especialidad');
// Carga de datos
$data = $Lista;//$pdf->LoadData('paises.txt');
$pdf->SetFont('Arial','',14);

$pdf->AddPage('L');
$pdf->FancyTableCita2($header,$Lista);
$pdf->Output();
?>



