<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
if (Session::NoExisteSesion("user")){
    header("location: ../login.php");
    return;
}

//EN EL CASO DE ACTUALIZAR LA PAGINA WEB ENTONCES LLAMAREMOS NUEVAMENTE
//AL CONTROLADOR PARA DE ESTA MANERA TENER ACTULIAZADA LA LISTA
//Y QUE NO ME GENERE NINGUN ERROR DE CARGA DE DATOS
if(Session::NoExisteSesion("listaEspecialidad")){
    header("location: ../../Controller/CitaController.php?Op=Listar");
    return;
}
if (Session::existeSesion("reservado")) {
    // $reserva = Session::getSesion("reservado");
    $reservo = 0;
    
}
 else {
     $reservo = 100;
}
//$Lista= Session::getSesion("lista");
//Session::eliminarSesion("lista");
$Usuario = Session::getSesion("user");
//estas variables se definen en una sola linea
//Llamamos al menu
$ListaEspecialidad=  Session::getSesion("listaEspecialidad");
Session::eliminarSesion("listaEspecialidad");
Layout::menu('', $Usuario, $reservo);

?>
<div class="row">
    <div class="col-lg-12">
        <div class="page-header">
        <h3 class="title-header">Reservar una Cita</h3>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading">
            Elija una Especialidad para ver su Horario y Reservar una Cita
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <?php
            $url ="../../Controller/CitaController.php?Op=Horario&idEsp=";
            foreach ($ListaEspecialidad as $row){
                if ($row['nombres'] != 'SIN ESPECIALIDAD'){ ?>
            <p>
                <button type="button"
                        onclick="location.href='<?php echo $url.$row['idespecialidad']."&nomEsp=".$row['nombres']?>'"
                        class="btn btn-outline btn-primary btn-sm btn-block">
                        <?php echo $row['nombres'];
                } ?>
                    
                </button>
            </p>
            <?php } ?>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
<?php
//LLAMAMOS AL FOOTER Y SE CIERRA LA PAGINA
Layout::footer();
?>