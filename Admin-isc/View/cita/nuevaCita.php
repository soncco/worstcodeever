<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
require_once './comboDinamico.php';
//require_once '../comboDinamico.php';
if (Session::NoExisteSesion("user")) {
  header("location: ../login.php");
  return;
}

if (Session::NoExisteSesion("citaEspecialidad")) {
  header("location: ../../Controller/CitaController.php?Op=Listar");
  return;
}
if (Session::existeSesion("reservado")) {
    //$reservado = Session::getSession("reservado");
    $reservo = 0;
}
else {
    $reservo = 100;
}
$Usuario= Session::getSesion("user");
$Lista= Session::eliminarSesion("citaEspecialidad");

//llamamos al menu
Layout::menu("", $Usuario, $reservo);
$nomEsp = $_REQUEST['nomEsp'];
$idEsp = $_REQUEST['idEsp'];
$url = "../../Controller/CitaController.php?idEsp=".$idEsp."&nomEsp=".$nomEsp;

// solo el contenido que cambiara ira aqui
?>
<div class="row">
    <div class="col-lg-12">
        <div class="page-header">
        <h3 class="title-header">Horario de <?php echo $nomEsp; //.".$idEsp ?>
            <span class="small pull-right">
            <a href="Cita.php" class="btn btn-info btn-sm">
                <span class="glyphicon glyphicon-hand-left"></span> Volver al Horario
            </a>
            </span>
        </h3>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="col-lg-12">
    
    <div class="panel panel-default">
        <div class="panel-heading">
            <?php echo $nomEsp ?>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>HORA</th>
                            <th>PACIENTE</th>
                            <th>DIA</th>
                            <th>ESTADO</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($Lista as $row) {
                            ?>
                        <tr>
                            <td><?php echo $row['horainicio']?> </td>
                            
                            <?php if($row['paciente']==""){?>
                            <td>
                                <form role="form" method="post" action="<?php echo $url ?>">
                                    <div class="form-group input-group">
                                        <input class="hidden-sm" type="text" name="idhorario" value="<?php echo $row['idhorario']?>" hidden>
                                        <input class="hidden-sm" type="text" name="usuario" value="<?php echo $Usuario['email']?>" hidden >
                                        <input class="hidden-sm" type="text" name="Op" value="Guardar" hidden >
                                        <span class="input-group-btn">
                                            <button class="btn btn-outline btn-success btn-block" type="submit" onclick="return confirm('¡Registrar Cita!, ¿Estas Seguro?');">
                                                RESERVAR
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </td>
                            <?php } else{ ?>
                            <td class="danger"><?php echo $row['paciente']?></td>
                            <?php } ?>
                            <td>
                                <?php echo $row['dia']; ?>
                            </td>
                            <td>
                                <?php
                                if($row['estado']==""){?>
                                LIBRE
                                <?php } else{ echo $row['estado'];
                                }?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.panle-body -->
    </div>
</div>
<?php
//llamamos al footer y se cierra la pagina
Layout::footer();
?>


