<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
if (Session::NoExisteSesion("user")) {
  header("location: ../login.php");
  return;
}

if (Session::NoExisteSesion("listaCitas")) {
  $Usuario = Session::getSesion("user");
  header("location; ../../Controller/CitaController.php?Op=MisCitasusuario=".$Usuario['email']);
  return;
}
$Lista = Session::getSesion("listaCitas");
Session::eliminarSesion("listaCitas");
$Usuario = Session::getSesion("user");
if (Session::existeSesion("reservado")) {
    // $reserva = Session::getSession("reservado");
    $reservo = 0;
}
 else {
     $reservo = 100;
}
//estas valibles se definen en una sola linea
$jsm = "<link href='../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css' rel='stylesheet'>";
$jsm.= "<link href='../../bower_components/datatables-responsive/css/dataTables.responsive.css' rel='stylesheet'>";
//llamamos al meenu
Layout::menu($jsm, $Usuario, $reservo);
$print = "../../Controller/CitaController.php?usuario=".$Usuario['email']."&Op=";
?>
<div class="row">
<div class="col-lg-12">
    
<!--    <h3 class="title-header">Mis Citas
            <a href="<?php echo $print."Nuevo";?>" class="btn btn-info btn-sm">
                <span class="glyphicon glyphicon-plus"></span> Agregar 
            </a>
            <a href="<?php echo $print."Print" ;?>" class="btn btn-success btn-sm" target="_blank">
                <span class="glyphicon glyphicon-print"></span> Imprimir 
            </a>            
        </h3>-->
    <div class="page-header">
        <h3 class="title-header">Mis Citas
            <span class="small pull-right">
        <a href="<?php echo $print."Print" ;?>" class="btn btn-success btn-sm" target="_blank">
                    <span class="glyphicon glyphicon-print"></span> Imprimir 
        </a></span>
        </h3>
    </div>
    <?php
    if (Session::existeSesion("mensaje")){
        $mensaje = Session::eliminarSesion("mensaje");
        if( $mensaje['Error'] == 0) {
            ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
        <?php echo "NRO DE ERRORES: ".$mensaje['Error'].", Mensaje: ".$mensaje['Mensaje']?>
    </div>
    <?php
        }
        else {
        ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
        <?php echo "NRO DE ERRORES: ".$mensaje['Error'].", MENSAJE: ".$mensaje['Mensaje']?>
    </div>
        <?php 
            }
    }
       ?>
</div>
<!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Relacion de Citas
            </div>
            <!-- /.panle-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Paciente</th>
                                <th>Fecha</th>
                                <th>Dia</th>
                                <th>Hora</th>
                                <th>Estado</th>
                                <th>Medico</th>
                                <th>Especialidad</th>
                                <th>Operaciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $url = "../../Controller/CitaController.php?Op=Cancelar&idcita=";
                            foreach ($Lista as $row ) {
                                ?>
                            <tr class="odd gradeX">
                                <td><?php echo $row['nompaciente']?></td>
                                <td><?php echo $row['fecha']?></td>
                                <td><?php echo $row['dia']?></td>
                                <td><?php echo $row['horainicio']?></td>
                                <td><?php echo $row['estado']?></td>
                                <td><?php echo $row['nomtrabajador']?></td>
                                <td><?php echo $row['nomespecialidad']?></td>
                                <td class="center">
                                    <ul class="nav nav-pills">
                                        <?php if($row['estado'] == 'RESERVADO') { ?>
                                        <li>
                                            <a href="<?php echo $url . $row ['idcita'] ?>&Op=Cancelar" title="Cancelar Cita" class="btn btn-info btn-sm" 
                                               
                                               onclick="return confirm('¡Esta accion anulara su atencion el dia de hoy!, ¿Estas Seguro?');">
                                                <span class="glyphicon glyphicon-erase"></span>CANCELAR </a>                                     
                                                
                                        </li>
                                        <?php } else { ?>
                                        <a href="#" class="btn btn-danger btn-sm" title="Opcion deshabilitada">
                                            <span class="glyphicon glyphicon-erase"></span> <?php echo $row ['estado']; ?>
                                        </a>
                                        <?php } ?>
                                    </ul>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
//llamamos al footer y se cierra la pagina

$jsf ="script src='../../bower-components/DataTables/media/js/jquery.dataTables.min.js'></script>";
$jsf .="<script src='../../bower-components/dattables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js'></script>";
Layout::footer($jsf);
?>