<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
if (Session::NoExisteSesion("user")){
    header("location: ../login.php");
    return;
}

//EN EL CASO DE ACTUALIZAR LA PAGINA WEB ENTONCES LLAMAREMOS NUEVAMENTE
//AL CONTROLADOR PARA DE ESTA MANERA TENER ACTULIAZADA LA LISTA
//Y QUE NO ME GENERE NINGUN ERROR DE CARGA DE DATOS
if(Session::NoExisteSesion("todoSanciones")){
    header("location: ../../Controller/SancionController.php?Op=Todas");
    return;
}
$Lista= Session::getSesion("todoSanciones");
Session::eliminarSesion("todoSanciones");
$Usuario = Session::getSesion("user");
//estas variables se definen en una sola linea
$jsm = "<link href='../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css' rel='stylesheet'>";
$jsm.= "<link href='../../bower_components/datatables-responsive/css/dataTables.responsive.css' rel='stylesheet'>";
//Llamamos al menu
Layout::menu($jsm, $Usuario);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="page-header">
        <h3 class="title-header">Todas las Sanciones
            <span class="small pull-right">
        <a href="<?php echo $print."Print" ;?>" class="btn btn-success btn-sm" target="_blank">
                <span class="glyphicon glyphicon-print"></span> Imprimir 
        </a>
        </span>
        </h3>
        </div>
        <?php 
        if (Session::existeSesion("mensaje")) {
            $mensaje = Session::eliminarSesion("mensaje");
             if ($mensaje['Error'] == 0) {
        ?>
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <?php echo "NRO DE ERRORES: ".$mensaje['Error'].", MENSAJE: ".$mensaje['Mensaje']?>
        </div>
        <?php
             }
        else{
         ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
            <?php echo "NRO DE ERRORES: ".$mensaje['Error'].", MENSAJE: ".$mensaje['Mensaje']?>
        </div>
        <?php
        }
        }
        ?>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Alta de pacientes Sancionados
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Paciente</th>
                                <th>Fecha Sancion</th>
                                <th>Fecha Vencimiento</th>
                                <th>Estado</th>
                                <th>Habilitar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $url = "../../Controller/SancionController.php?Op=Habilitar&idsancion=";
                            foreach ($Lista as $row ) {
                            ?>
                            <tr class="odd gradeX">
                                <td><?php echo $row ['nombres']?></td>
                                <td><?php echo $row ['fechasancion']?></td>
                                <td><?php echo $row ['fechavencimiento']?></td>
                                <td class="center">
                                    <ul class="nav nav-pills">
                                        <?php if($row['estado'] == '1') {?>
                                        <li>
                                            <a href="" class="btn btn-danger btn-sm disabled" >
                                                <span class="fa fa-lock"></span></a>
                                            
                                        </li>
                                        <?php }elseif ($row['estado'] == '0') {?>
                                        <li>
                                            <a href="#" class="btn btn-success btn-sm disabled" >
                                                <span class="fa fa-unlock"></span>
                                            </a>
                                        </li>
                                        <?php }?>
                                    </ul>
                                </td>
                                <td class="center">
                                    <ul class="nav nav-pills">
                                        <?php if($row['estado'] == '1') {?>
                                        <li>
                                            <a href="<?php echo $url . $row['idsancion']?>&estado=0" title="Habilitar" class="btn btn-info btn-sm"
                                               onclick="return confirm('¿Desea Habilitar a este paciente?');" rol="button" >
                                                <span class="fa fa-check"></span></a>
                                        </li>
                                        <?php }elseif ($row['estado'] == '0') {?>
                                        <li>
                                            <a href="#" class="btn btn-warning btn-sm disabled" >
                                                <span class="fa fa-ban"></span>
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
//Llamamos al footer y se cierra la pagina
$jsf = "<script src='../../bower_components/DataTables/media/js/jquery.dataTables.min.js'></script>";
$jsf.="<script src='../../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js'></script>";
Layout::footer($jsf);
?>