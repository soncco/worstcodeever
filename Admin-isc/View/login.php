<?php
    session_start();
    require_once '../util/Sesion.php';
    if(Session::existeSesion("error")){
        $error = Session::eliminarSesion("error");
        $usuario = Session::eliminarSesion("usuario");
    }
    else {
        $error = "";
   } 
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>UNSAAC-Reserva de Citas</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<!--<link rel="stylesheet" href="../../Admin-isc/css/estilo.css">
<script src="js/modernizr.custom.97632.js"></script>
<script src="js/prefixfree.min.js"></script>
<img id="bg" src="../img/unsaac.jpg" alt="Fondo" />-->
<body>

    <div class="background-container">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h1>Bienvenido</h1>
                    <div class="panel-body">
                        <form role="form" method="post" action="../Controller/LoginController.php">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="pass" type="password" value="">
                                </div>
                                <div class="form-group">                                    
                                    <input name="Op" type="text" value="Autenticar" hidden="">                                   
                                </div>
                                <div class="form-group">        
                                    <button type="submit" class="btn btn-sm btn-success btn-block">Ingresar</button>                                    
                                </div>
                                <h5> Eres nuevo paciente? ->  <a class="button" href="<?php echo"./../View/paciente/nuevoPaciente.php";?>">REGISTRATE?</a>
                               </h5>
                            </fieldset>
                        </form>
                        <?php
                            if($error <> NULL){                                                     
                               echo "<div class='alert alert-danger alert-dismissable'>"
                                . "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>"
                                       . "$error</div>";
                            }
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <footer class="footer navbar-default">
        <p class="text-center">Reserva de Citas - &copy; ISC 2016</p>
    </footer>

  
    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>

</body>

</html>
