<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
require_once './comboDinamico.php';
if(Session::NoExisteSesion("user") ) {
    header("location: ../login.php");
    return;
}
if(Session::NoExisteSesion("listaEspecialidad") ) {
    $idEdit=$_REQUEST['cod'];
    header("location: ../../Controller/TrabajadorController.php?Op=Recuperar&id=$idEdit");
    return;
}
$Usuario = Session::getSesion("user");
//Llamamos al menu
Layout::menu("", $Usuario);
$url = "../../Controller/TrabajadorController.php";
$UpperCss = "style='text-transform:uppercase;'";
$UpperJs = "onkeyup='javascript:this.value=this.value.toUpperCase();'";
//Solo el contenido que cambiara ira aqui
$ListaEspecialidad=  Session::getSesion("listaEspecialidad");
Session::eliminarSesion("listaEspecialidad");

?>
<div class="row">
    <div class="col-lg-12">
        <div class="page-header">
        <h3 class="title-header">Modificar Trabajador
            <img src="<?php echo "files/".$_REQUEST['foto']?>"  class="img-thumbnail" width="54" height="26">
            <span class="small pull-right">
                <a href="Trabajador.php" class="btn btn-info btn-sm">
                    <span class="glyphicon glyphicon-hand-left"></span> Volver 
                </a>
            </span>
        </h3>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Modifique la informacion del Trabajador
        </div>
        <div class="panel-body">
            <div class="row">
                <form role="form" method="post" enctype="multipart/form-data" action="<?php echo $url; ?>">
                <div class="col-lg-6">  
                    <div class="form-group">
                        <label>Cod Trabajador</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> disabled="" value="<?php echo $_REQUEST['cod']?>" >
                        <input class="hidden-sm"type="text" name="cod" value="<?php echo $_REQUEST['cod']?>" class="form-control" hidden >
                        <input class="hidden-sm"type="text" name="foto" value="<?php echo $_REQUEST['foto']?>" class="form-control" hidden >
                        <input class="hidden-sm"type="text" value="Editar" class="form-control" hidden name="Op" >
                    </div>
                    <div class="form-group">
                        <label>Nombre del Trabajador</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="nombres" autofocus type="text" value="<?php echo $_REQUEST['nom']?>">
                    </div>
                    <div class="form-group">
                        <label>DNI</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="dni" type="number" value="<?php echo $_REQUEST['dni']?>" >
                    </div>
                    <div class="form-group">
                        <label>Direccion</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="direccion" type="text" value="<?php echo $_REQUEST['dir']?>">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="email" type="email" value="<?php echo $_REQUEST['ema']?>" >
                    </div>                    
                </div>                
                <div class="col-lg-6">                   
                    <div class="form-group">
                        <label>Elija el Cargo</label>
                        <select class="form-control" name="cargo">
                            <option value="TRABAJADOR" <?php if($_REQUEST['cargo'] == "TRABAJADOR"){ echo 'selected="selected"';} ?> >TRABAJADOR</option>
                            <option value="MEDICO" <?php if($_REQUEST['cargo'] == "MEDICO"){ echo 'selected="selected"';} ?>>MEDICO</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Telefono</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="telefono" type="tel" value="<?php echo $_REQUEST['tel']?>">
                    </div>                         
                    <div class="form-group">
                        <label>Solo si desea cambiar la Foto</label>
                        <input type="file" name="archivo" >
                    </div>
                    <div class="form-group">
                        <label>Elija la Especialidad</label>
                        <?php $idTipo = $_REQUEST['idesp']; comboBoxEspecialidadSelect($ListaEspecialidad,$idTipo);?>
                    </div>
                    <button type="submit" class="btn btn-success">Guardar</button>
                    <button type="reset" class="btn btn-warning">Reset</button>
                    
                </div>
                </form>
            </div>    
        </div>
    </div>
</div>
<?php
//Llamamos al footer y se cierra la pagina
Layout::footer();
?>     