<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
if(Session::NoExisteSesion("user") ) {
    header("location: ../login.php");
    return;
}
$Usuario = Session::getSesion("user");
//Llamamos al menu
Layout::menu("", $Usuario);
$url = "Trabajador.php";
//Solo el contenido que cambiara ira aqui
?>
<div class="row">
    <div class="col-lg-12">
        <div class="page-header">
        <h3 class="title-header">Ver Trabajador
            <span class="pull-right span">
        <a href="<?php echo $url;?>" class="btn btn-info btn-sm">
            <span class="glyphicon glyphicon-hand-left"></span> Volver 
        </a></span>
        </h3></div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Solo se visualiza informacion del trabajador
        </div>
        <div class="panel-body">
            <div class="row">
                <form role="form"  action="<?php echo $url; ?>">
                <div class="col-lg-6">                    
                    <div class="form-group">
                        <label>Codigo Trabajador</label>
                        <input class="form-control" disabled="" value="<?php echo $_REQUEST['cod']?>" type="text">
                    </div>
                    <div class="form-group">
                        <label>Nombre del Trabajador</label>
                        <input class="form-control" disabled="" value="<?php echo $_REQUEST['nom']?>" type="text">
                    </div>
                    <div class="form-group">
                        <label>DNI</label>
                        <input class="form-control"  disabled="" value="<?php echo $_REQUEST['dni']?>" type="text">
                    </div>
                    <div class="form-group">
                        <label>Direccion</label>
                        <input class="form-control"  disabled="" value="<?php echo $_REQUEST['dir']?>" type="text">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input class="form-control"  disabled="" value="<?php echo $_REQUEST['ema']?>" type="text">
                    </div> 
                    <div class="form-group">
                        <label>Telefono</label>
                        <input class="form-control" disabled="" value="<?php echo $_REQUEST['tel']?>" type="text">
                    </div>
                </div>                
                <div class="col-lg-6">   
                    <div class="form-group">
                        <label>Foto</label>
                        <img src="<?php echo "files/".$_REQUEST['foto']?>"  class="img-thumbnail" alt="Cinque Terre" width="204" height="136">
                    </div>
                    <div class="form-group">
                        <label>Cargo</label>
                        <input class="form-control"  disabled="" value="<?php echo $_REQUEST['cargo']?>">
                    </div>
                                           
                    
                    <div class="form-group">
                        <label>Especialidad</label>
                        <input class="form-control" disabled="" value="<?php echo $_REQUEST['esp']?>">
                    </div>
                                        
                </div>
                </form>    
        </div>
    </div>
</div>
<?php Layout::footer(); ?>       

