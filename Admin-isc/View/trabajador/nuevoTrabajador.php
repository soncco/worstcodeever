<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
require_once './comboDinamico.php';
if(Session::NoExisteSesion("user") ) {
    header("location: ../login.php");
    return;
}
if(Session::NoExisteSesion("listaEspecialidad") ) {
    header("location: ../../Controller/TrabajadorController.php?Op=Nuevo");
    return;
}
$Usuario = Session::getSesion("user");
//Llamamos al menu
Layout::menu("", $Usuario);
$url = "../../Controller/TrabajadorController.php?Op=Guardar";
$UpperCss = "style='text-transform:uppercase;'";
$UpperJs = "onkeyup='javascript:this.value=this.value.toUpperCase();'";
//Solo el contenido que cambiara ira aqui
$ListaEspecialidad=  Session::getSesion("listaEspecialidad");
Session::eliminarSesion("listaEspecialidad");
$url1 = "Trabajador.php";
?>
<div class="row">
    <div class="col-lg-12">
        <div class="page-header">
        <h3 class="title-header">Nuevo Trabajador
            <span class="small pull-right">
            <a href="<?php echo $url1;?>" class="btn btn-info btn-sm">
                <span class="glyphicon glyphicon-hand-left"></span> Volver 
            </a>
            </span>
        </h3>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            Ingrese un nuevo Trabajador
        </div>
        <div class="panel-body">
            <div class="row">
                <form role="form" method="post" enctype="multipart/form-data" action="<?php echo $url; ?>">
                <div class="col-lg-6">                    
                    <div class="form-group">
                        <label>Nombre del Trabajador</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="nombres" autofocus type="text">
                    </div>
                    <div class="form-group">
                        <label>DNI</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="dni" type="number">
                    </div>
                    <div class="form-group">
                        <label>Direccion</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="direccion" type="text">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="email" type="email">
                    </div>                    
                </div>                
                <div class="col-lg-6">                   
                    <div class="form-group">
                        <label>Elija el Cargo</label>
                        <select class="form-control" name="cargo">
                            <option value="" selected="selected">-- ELIGE UN CARGO</option>
                            <option value="TRABAJADOR">TRABAJADOR</option>
                            <option value="MEDICO">MEDICO</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Telefono</label>
                        <input class="form-control" <?php echo $UpperCss.' '.$UpperJs;?> name="telefono" type="tel">
                    </div>                         
                    <div class="form-group">
                        <label>Subir Foto</label>
                        <input type="file" name="archivo" >
                    </div>
                    <div class="form-group">
                        <label>Elija la Especialidad</label>
                        <?php comboBoxEspecialidad($ListaEspecialidad);?>
                    </div>
                    <button type="submit" class="btn btn-success">Guardar</button>
                    <button type="reset" class="btn btn-warning">Reset</button>
                    
                </div>
                </form>
            </div>    
        </div>
    </div>
</div>
<?php
//Llamamos al footer y se cierra la pagina
Layout::footer();
?>                