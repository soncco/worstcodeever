<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Model/PDF.php';;
if(Session::NoExisteSesion("user") ) {
    header("location: ../login.php");
    return;
}
if(Session::NoExisteSesion("print") ) {
    header("location: ../../Controller/TrabajadorController.php?Op=Listar");
    return;
}
$Lista= Session::getSesion("print");
Session::eliminarSesion("print");
//print_r($Lista);

$pdf = new PDF();
$pdf->AliasNbPages();
// Títulos de las columnas
//$header = array('Id','Nombres','Dni','Direccion','Email','Cargo','Telefono','Especialidad');
$header = array('Id','Nombres','Dni','Cargo','Especialidad');
// Carga de datos
$data = $Lista;//$pdf->LoadData('paises.txt');
$pdf->SetFont('Arial','',14);

$pdf->AddPage();
$pdf->FancyTableTrab($header,$Lista);
$pdf->Output();
?>



