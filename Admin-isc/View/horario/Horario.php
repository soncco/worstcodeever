<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
if(Session::NoExisteSesion("user") ) {
    header("location: ../login.php");
    return;
}
//En el caso de actualizar la pagina web entonces llamaremos nuevamente
//al Controlador para de esta manera tener actualizada la lista
//y que no me genere ningun error de carga de datos
if(Session::NoExisteSesion("listaEspecialidad") ) {
    header("location: ../../Controller/HorarioController.php?Op=Listar");
    return;
}
//$Lista= Session::getSesion("lista");
//Session::eliminarSesion("lista");
$Usuario = Session::getSesion("user");
//estas variables se definen en una sola linea
//Llamamos al menu
$ListaEspecialidad=  Session::getSesion("listaEspecialidad");
Session::eliminarSesion("listaEspecialidad");
Layout::menu('', $Usuario);

?>
<div class="row">
    <div class="col-lg-12">
        <div class="page-header">
            <h3 class="title-header">Horarios de Especialidades</h3>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Elija una Especialidad para ver su Horario
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
            <?php
            $url = "../../Controller/HorarioController.php?Op=Horario&idEsp=";
            foreach ($ListaEspecialidad as $row ) {
                if($row['nombres'] != 'SIN ESPECIALIDAD'){
                ?>
                    <p>
                    <button type="button" onClick="location.href='<?php echo $url.$row['idespecialidad']."&nomEsp=".$row['nombres'] ?>'"
                    class="btn btn-outline btn-primary btn-sm btn-block">
                <?php      echo $row['nombres'];?>
                <?php } ?>
                    </button>
                    </p>
            <?php } ?>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php
//Llamamos al footer y se cierra la pagina
Layout::footer();
?>
