<?php
session_start();
require_once '../../util/Sesion.php';
require_once '../../Layout/Layout.php';
require_once './comboDinamico.php';
//require_once '../comboDinamico.php';
if (Session::NoExisteSesion("user")) {
  header("location: ../login.php");
  return;
}

if (Session::NoExisteSesion("horarioEspecialidad")) {
  header("location: ../../Controller/HorarioController.php?Op=Listar");
  return;
}
if (Session::NoExisteSesion("trabajadorEspecialidad")) {
  header("location: ../../Controller/HorarioController.php?Op=Listar");
  return;
}

$Usuario= Session::getSesion("user");
$Lista= Session::eliminarSesion("horarioEspecialidad");
$ListaTrabEsp= Session::eliminarSesion("trabajadorEspecialidad");


//Llamamos al menu
Layout::menu("", $Usuario);
$nomEsp = $_REQUEST['nomEsp'];
$idEsp = $_REQUEST['idEsp'];
$url = "../../Controller/HorarioController.php?idEsp=".$idEsp."&nomEsp=".$nomEsp;
//solo el contenido que cambiara ira aqui
?>
<div class="row">
  <div class="col-lg-12">
    <div class="page-header">
    <h3 class="title-header">Horario de <?php  echo $nomEsp; //.".$idEsp ?>
      <span class="pull-right small">
      <a href="Horario.php" class="btn btn-info btn-sm">
        <span class="glyphicon glyphicon-hand-left"></span> Volver al Horario
      </a>
      </span>
    </h3>
    </div>
  </div>
  <!-- /.col-lg-12 -->
</div>
<div class="col-lg-12">

  <div class="panel panel-default">
    <div class="panel-heading">
      <?php echo $nomEsp; ?>
    </div>
    <!-- /.panel-heading -->
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-striped table-bordered table-hover">
          <thead>
            <tr>
              <th>Hora</th>
              <th>LUNES</th>
              <th>MARTES</th>
              <th>MIERCOLES</th>
              <th>JUEVES</th>
              <th>VIERNES</th>
            </tr>
          </thead>
          <tbody>
            <?php
            
            foreach ($Lista as $row){
             ?>
            <tr>
              <td><?php echo $row['horainicio']?></td>
              <td>
                <?php if ($row['lunes']=="") {?>
                <form role="form" method="post" action="<?php echo $url ?>">
                <div class="form-group input-group">
                <?php comboBoxTrabajador($ListaTrabEsp) ?>
              <input class="hidden-sm" type="text" name="dia" value="LUNES" hidden>
                  <input class="hidden-sm" type="text" name="hora" value="<?php echo $row['horainicio']?>" hidden>
                  <input class="hidden-sm" type="text" name="Op" value="Guardar" hidden>
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" onclick="return confirm('¡Registrar Horario!, ¿Estas seguro?');">
                    <i class="fa fa-thumbs-o-up"></i>
                  </button>
                </span>
              </div>
            </form>
            <?php } else { ?>
              <form role="form" method="post" action="<?php echo $url ?>">
                  <div class="form-group input-group">
                    <input class="form-control" type="text" value="<?php echo $row['lunes']?>" disabled="">
                    <input class="hidden-sm" type="text" name="nom" value="<?php echo $row['lunes']?>" hidden >
                    <input class="hidden-sm" type="text" name="dia" value="LUNES" hidden>
                    <input class="hidden-sm" type="text" name="hora" value="<?php echo $row['horainicio']?>" hidden>
                    <input class="hidden-sm" type="text" name="Op" value="Liberar" hidden>
                    <span class="input-group-btn">
                      <button class="btn btn-defaulf" type="submit" onclick="return confirm('¡Liberar Horario!, ¿Estas seguro, esta acccion es irreversible!!?');">
                      <i class="fa fa-thumbs-o-down"></i>
                    </button>
              </span>
            </div>
          </form>
          <?php } ?>
        </td>
          <td>
                <?php if ($row['martes']=="") {?>
                <form role="form" method="post" action="<?php echo $url ?>">
                <div class="form-group input-group">
                <?php comboBoxTrabajador($ListaTrabEsp) ?>
              <input class="hidden-sm" type="text" name="dia" value="MARTES" hidden>
                  <input class="hidden-sm" type="text" name="hora" value="<?php echo $row['horainicio']?>" hidden>
                  <input class="hidden-sm" type="text" name="Op" value="Guardar" hidden>
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="submit" onclick="return confirm('¡Registrar Horario!, ¿Estas seguro?');">
                    <i class="fa fa-thumbs-o-up"></i>
                  </button>
                </span>
              </div>
            </form>
            <?php } else { ?>
              <form role="form" method="post" action="<?php echo $url ?>">
                  <div class="form-group input-group">
                    <input class="form-control" type="text" value="<?php echo $row['martes']?>" disabled="">
                    <input class="hidden-sm" type="text" name="nom" value="<?php echo $row['martes']?>" hidden >
                    <input class="hidden-sm" type="text" name="dia" value="MARTES" hidden>
                    <input class="hidden-sm" type="text" name="hora" value="<?php echo $row['horainicio']?>" hidden>
                    <input class="hidden-sm" type="text" name="Op" value="Liberar" hidden>
                    <span class="input-group-btn">
                      <button class="btn btn-defaulf" type="submit" onclick="return confirm('¡Liberar Horario!, ¿Estas seguro, esta acccion es irreversible!!?');">
                      <i class="fa fa-thumbs-o-down"></i>
                    </button>
              </span>
            </div>
          </form>
          <?php } ?>
        </td>                          
     
            <td>
            <?php if($row['miercoles']=="") { ?>
              <form role="form" method="post" action="<?php echo $url ?>">
              <div class="form-group input-group">
              <?php comboBoxTrabajador($ListaTrabEsp) ?>
              <input class="hidden-sm" type="text" name="dia" value="MIERCOLES" hidden>
                <input class="hidden-sm" type="text" name="hora" value="<?php echo $row['horainicio']?>" hidden >
                <input class="hidden-sm" type="text" name="Op" value="Guardar" hidden>
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit" onclick="return confirm('¡Registrar Horario!, ¿Estas seguro?');">
                  <i class="fa fa-thumbs-o-up"></i>
                </button>
              </span>
            </div>
            </form>
            <?php } else{  ?>
              <form role="form" method="post" action="<?php echo $url ?>">
              <div class="form-group input-group">
                <input class="form-control" type="text" value="<?php echo $row['miercoles']?>" disabled="">
                <input class="hidden-sm" type="text" name="nom" value="<?php echo $row['miercoles']?>" hidden>
                <input class="hidden-sm" type="text" name="dia" value="MIERCOLES" hidden>
                <input class="hidden-sm" type="text" name="hora" value="<?php echo $row['horainicio']?>" hidden>
                <input class="hidden-sm" type="text" name="Op" value="Liberar" hidden>
                <span class="input-group-btn">
                  <button class="btn btn-defaulf" type="submit" onclick="return confirm('¡Liberar Horario!, ¿Estas seguro, esta acccion es irreversible!!?');">
                  <i class="fa fa-thumbs-o-down"></i>
                </button>
              </span>
            </div>
        </form>
        <?php  } ?>
            </td>
            <td>
            <?php if($row['jueves']=="") { ?>
              <form role="form" method="post" action="<?php echo $url ?>">
              <div class="form-group input-group">
                  <?php comboBoxTrabajador($ListaTrabEsp) ?>
              <input class="hidden-sm" type="text" name="dia" value="JUEVES" hidden>
                <input class="hidden-sm" type="text" name="hora" value="<?php echo $row['horainicio']?>" hidden >
                <input class="hidden-sm" type="text" name="Op" value="Guardar" hidden>
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit" onclick="return confirm('¡Registrar Horario!, ¿Estas seguro?');">
                  <i class="fa fa-thumbs-o-up"></i>
                </button>
              </span>
            </div>
            </form>
            <?php } else {  ?>
              <form role="form" method="post" action="<?php echo  $url ?>">
              <div class="form-group input-group">
                <input class="form-control" type="text" value="<?php echo $row['jueves']?>" disabled="">
                <input class="hidden-sm" type="text" name="nom" value="<?php echo $row['jueves']?>" hidden>
                <input class="hidden-sm" type="text" name="dia" value="JUEVES" hidden>
                <input class="hidden-sm" type="text" name="hora" value="<?php echo $row['horainicio']?>" hidden>
                <input class="hidden-sm" type="text" name="Op" value="Liberar" hidden>
                <span class="input-group-btn">
                  <button class="btn btn-defaulf" type="submit" onclick="return confirm('¡Liberar Horario!, ¿Estas seguro, esta acccion es irreversible!!?');">
                  <i class="fa fa-thumbs-o-down"></i>
                </button>
              </span>
            </div>
            </form>
            <?php  } ?>
            </td>
            
            <td>
            <?php if($row['viernes']=="") { ?>
              <form role="form" method="post" action="<?php echo $url ?>">
              <div class="form-group input-group">
                  <?php comboBoxTrabajador($ListaTrabEsp) ?>
              <input class="hidden-sm" type="text" name="dia" value="VIERNES" hidden>
                <input class="hidden-sm" type="text" name="hora" value="<?php echo $row['horainicio']?>" hidden >
                <input class="hidden-sm" type="text" name="Op" value="Guardar" hidden>
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit" onclick="return confirm('¡Registrar Horario!, ¿Estas seguro?');">
                  <i class="fa fa-thumbs-o-up"></i>
                </button>
              </span>
            </div>
            </form>
            <?php } else {  ?>
              <form role="form" method="post" action="<?php echo  $url ?>">
              <div class="form-group input-group">
                <input class="form-control" type="text" value="<?php echo $row['viernes']?>" disabled="">
                <input class="hidden-sm" type="text" name="nom" value="<?php echo $row['viernes']?>" hidden>
                <input class="hidden-sm" type="text" name="dia" value="VIERNES" hidden>
                <input class="hidden-sm" type="text" name="hora" value="<?php echo $row['horainicio']?>" hidden>
                <input class="hidden-sm" type="text" name="Op" value="Liberar" hidden>
                <span class="input-group-btn">
                  <button class="btn btn-defaulf" type="submit" onclick="return confirm('¡Liberar Horario!, ¿Estas seguro, esta acccion es irreversible!!?');">
                  <i class="fa fa-thumbs-o-down"></i>
                </button>
              </span>
            </div>
            </form>
            <?php  } ?>
            </td>

            </tr>
           <?php  } ?>
        </tbody>
        </table>
          </div>
          <!-- /.table-responsive -->
        </div>
          <!-- /.table-responsive -->
      </div>
</div>

<?php
//llamamos al footer y se cierra la pagina
Layout::footer();
 ?>